# Requirements
Latest version of __Anaconda__ or __Python 3.7.7__

# Install dependencies
__anaconda users:__  
```
conda env create -f environment.yml
```  
__non-anaconda users:__  
```
pip install -r requirements.txt
```

# How to run
Anaconda users will have to activate the environment prior to running the application.
```
conda activate manseg
```
```
python manseg.py
```

# How to update dependencies
```
conda env update -f environment.yml
```

# How to run programmatically
Refer to the `example.py` script. Currently the programmatic approach is the only way to add existing layers.

# Key bindings
| Key        | Description                                  |
|------------|----------------------------------------------|
|    `Esc`   | Brings up the load image dialog.             |
| `ctrl + s` | Saves the current working state of the app.  |
| `ctrl + l` | Loads the previously saved state of the app. |
| `ctrl + e` | Exports the current ROI's.                   |

# Exporting ROI's
By using the `ctrl + e` key binding, a user is able to export all ROI's that the user has added to the application. The ROI's will be found in the current working directory under a special directory called `rois`.
Within this directory, the user will find a series of directories corresponding to the ROI's and their metadata. The naming convention follow the following structure:

```<roi-index>_<x-position>_<y-position>_<x-size>_<y-size>```

The following is an example directory structure:
```roi-index: 0
x-position: 100
y-position: 200
x-size: 800
y-size: 800

rois
|-- 0_100_200_800_800
|    |-- crops
|    |    |-- slice0000_100_200_800_800.png
|    |    |-- slice0001_100_200_800_800.png
|    |-- layers
|    |    |-- membranes
|    |    |    |-- slice0000_100_200_800_800.png
|    |    |    |-- slice0001_100_200_800_800.png
|    |    |-- mitochondria
|    |    |    |-- slice0000_100_200_800_800.png
|    |    |    |-- slice0001_100_200_800_800.png
```
