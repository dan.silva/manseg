from manseg import MansegApp

slice_images_path = '~/school/dse/260/jupyter/dat1/em_roi/'
pred_images_path = '~/school/dse/260/jupyter/dat1/predicted_membranes/3fm/'

slice_image_names = ['slice00070.png', 'slice00071.png', 'slice00072.png', 'slice00073.png', 'slice00074.png',
                     'slice00075.png', 'slice00076.png', 'slice00077.png', 'slice00078.png', 'slice00079.png',
                     'slice00080.png', 'slice00081.png', 'slice00082.png']
pred_image_names = ['Segmented_0006.png', 'Segmented_0007.png', 'Segmented_0008.png', 'Segmented_0009.png',
                    'Segmented_0010.png', 'Segmented_0011.png', 'Segmented_0012.png', 'Segmented_0013.png',
                    'Segmented_0014.png', 'Segmented_0015.png', 'Segmented_0016.png', 'Segmented_0017.png',
                    'Segmented_0018.png']

slices = [slice_images_path + n for n in slice_image_names]
pred = [pred_images_path + n for n in pred_image_names]

app = MansegApp()
app.process_images(slices, [pred])
app.recreate_ui()
app.run()
