import os
import pickle
import shutil
from glob import glob
from pathlib import Path
from threading import Thread
from typing import List, Optional, Tuple

import numpy as np
import pygame
from natsort import natsorted
from pygame.locals import HWSURFACE, DOUBLEBUF, RESIZABLE
from pygame_gui import UIManager, UI_FILE_DIALOG_PATH_PICKED
from pygame_gui.windows import UIMessageWindow
from skimage.io import imread, imsave

from components import ImageDraw, ImageExplorer, Sidebar, PreviewScreen, LayerDialog, \
    LoadingMessageWindow, FileImportDialog
from constants import EVENT_IMAGE_EXPLORER_PAN, EVENT_IMAGE_DRAW_BRUSH_UP, EVENT_IMAGE_DRAW_BRUSH_DOWN, \
    EVENT_SIDEBAR_IMAGE_SLIDER_MOVED, \
    EVENT_SIDEBAR_SELECT_LAYER, EVENT_SIDEBAR_BUTTON_REMOVE_LAYER, EVENT_SIDEBAR_BUTTON_ADD_LAYER, \
    EVENT_SIDEBAR_ALPHA_SLIDER_MOVED, EVENT_ROI_SELECTION_PREVIEW, MIN_WINDOW_RESOLUTION, SIDEBAR_WIDTH, \
    EVENT_ROI_SELECTION_ADD, EVENT_SIDEBAR_BUTTON_ADD_ROI, EVENT_SIDEBAR_SELECT_ROI, DEFAULT_WINDOW_RESOLUTION, \
    DEFAULT_ROI_RESOLUTION, DEFAULT_ROI_POSITION, EVENT_SIDEBAR_BRUSH_SLIDER_MOVED, EVENT_SIDEBAR_BUTTON_REMOVE_ROI, \
    EVENT_SIDEBAR_BUTTON_ERASE_BUTTON, EVENT_SIDEBAR_BUTTON_DRAW_BUTTON, EVENT_LAYER_DIALOG_CREATE, \
    EVENT_SIDEBAR_BUTTON_EDIT_LAYER, EVENT_LAYER_DIALOG_UPDATE, LAYER_OPTIONS, EVENT_ROI_SELECTION_CANCEL, TRANS, \
    EVENT_EXPORT_IMAGES_COMPLETE, EVENT_IMPORT_IMAGES_COMPLETE, SAVE_STATE_PATH, SAVE_STATE_ROI_PATH, \
    EVENT_SAVE_STATE_COMPLETE, EVENT_LOAD_STATE_COMPLETE, EVENT_LOAD_STATE_NOT_FOUND, OLD_SAVE_STATE_PATH


# designates SDL to set the pygame window to the center
# os.environ['SDL_VIDEO_CENTERED'] = '1'


class RoiOption:
    def __init__(self,
                 size: Tuple[int, int] = None,
                 position: Tuple[int, int] = None,
                 num_images: int = None):
        self.size = size
        self.position = position
        self.num_images = num_images
        self.layer_names: List[str] = []
        self.layer_types: List[str] = []
        self.layers: List[List[pygame.Surface]] = []

    def get_size(self):
        return self.size

    def get_position(self):
        return self.position

    def get_layer_name(self, index: int):
        return self.layer_names[index]

    def get_layer_names(self):
        return self.layer_names

    def set_layer_name(self, index: int, name: str):
        self.layer_names[index] = name

    def get_layer_type(self, index: int):
        return self.layer_types[index]

    def get_layer_types(self):
        return self.layer_types

    def set_layer_type(self, index: int, layer_type: str):
        self.layer_types[index] = layer_type

    def get_layer_count(self):
        return len(self.layer_names)

    def get_layer(self, index: int):
        """
        :param index: index of the requested layer
        :return: Returns a list of pygame.Surface corresponding to a single layer for every available image. If there
        are no layers, then it will return an empty List.
        """
        return self.layers[index]

    def add_layer(self, name: str, layer_type: str, layer_surface: List[pygame.Surface] = None):
        self.layer_names.append(name)
        self.layer_types.append(layer_type)
        layer_to_add = layer_surface
        if layer_surface is None:
            empty_surface = pygame.Surface(self.size)
            empty_surface.set_colorkey(TRANS)
            layer_to_add = [empty_surface.copy() for _ in range(self.num_images)]
        self.layers.append(layer_to_add)

    def remove_layer(self, index):
        self.layer_names.pop(index)
        self.layer_types.pop(index)
        self.layers.pop(index)

    def add_image(self):
        self.num_images += 1
        empty_surface = pygame.Surface(self.size)
        empty_surface.set_colorkey(TRANS)
        for layer in self.layers:
            layer.append(empty_surface)

    def get_layers_for_image(self, image_index: int):
        """
        :param image_index: index of the image
        :return: Returns a list of pygame.Surface corresponding to all layers for a given image. If there
        are no layers, then it will return an empty List.
        """
        if len(self.layer_names) > 0:
            return [layer[image_index] for layer in self.layers]
        return []

    def save_to_disk(self, roi_index):
        roi_path = os.path.join(SAVE_STATE_ROI_PATH, str(roi_index))
        Path(roi_path).mkdir(parents=True, exist_ok=True)
        f = open(os.path.join(roi_path, 'roi_option'), 'wb')
        pickle.dump(self.size, f)
        pickle.dump(self.position, f)
        pickle.dump(self.num_images, f)
        pickle.dump(self.layer_names, f)
        pickle.dump(self.layer_types, f)
        f.close()

        # layers as images
        for name, layer_type, layer in zip(self.layer_names, self.layer_types, self.layers):
            layer_path = os.path.join(roi_path, name)
            Path(layer_path).mkdir(parents=True, exist_ok=True)
            for surface_index, surface in enumerate(layer):
                # pygame.image.save seems to be a blocking call
                # pygame.image.save(surface, '{}/{}.png'.format(layer_path, str(surface_index)))
                imsave(os.path.join(layer_path, '{}.png'.format(str(surface_index))),
                       pygame.surfarray.array3d(surface.copy()))

    def load_from_disk(self, roi_index):
        roi_path = os.path.join(SAVE_STATE_ROI_PATH, str(roi_index))
        f = open(os.path.join(roi_path, 'roi_option'), 'rb')
        self.size = pickle.load(f)
        self.position = pickle.load(f)
        self.num_images = pickle.load(f)
        self.layer_names = pickle.load(f)
        self.layer_types = pickle.load(f)
        f.close()

        # read in images and convert to surfaces
        for name in self.layer_names:
            layer = []
            image_paths = natsorted(glob(os.path.join(roi_path, name, '*')))
            for image_path in image_paths:
                print(image_path)
                image = imread(image_path)
                # surface = pygame.image.load(image_path)
                surface = pygame.surfarray.make_surface(image)
                surface.set_colorkey(TRANS)
                layer.append(surface.convert())
            self.layers.append(layer)


class MansegApp:
    def __init__(self):
        pygame.init()
        pygame.display.set_caption('manseg')
        # self.resolution = pygame.display.list_modes()[0]
        self.resolution = DEFAULT_WINDOW_RESOLUTION
        self.roi_resolution = DEFAULT_ROI_RESOLUTION
        self.roi_position = DEFAULT_ROI_POSITION
        self.window_surface: pygame.Surface = pygame.display.set_mode(self.resolution,
                                                                      HWSURFACE | DOUBLEBUF | RESIZABLE)
        self.resolution = self.window_surface.get_size()
        self.background_surface = None
        self.ui_manager = UIManager(self.window_surface.get_size())

        self.sidebar: Optional[Sidebar] = None
        self.preview_screen: Optional[PreviewScreen] = None
        self.image_explorer: Optional[ImageExplorer] = None
        self.image_draw: Optional[ImageDraw] = None

        self.file_import_dialog: Optional[FileImportDialog] = None

        self.draw_mode_enabled = False
        self.image_index = 0
        self.image_surfaces: Optional[List[pygame.Surface]] = []
        self.layer_surfaces: Optional[List[List[pygame.Surface]]] = []

        self.image_names: Optional[List[str]] = []

        # contains all user defined roi's
        self.roi_options: List[RoiOption] = []
        self.selected_roi_index = None

        self.clock = pygame.time.Clock()
        self.loading_message_window_thread: Optional[Thread] = None
        self.import_thread: Optional[Thread] = None
        self.loading_message_window: Optional[UIMessageWindow] = None
        self.running = True
        self.debug_mode = False

    def save_state(self):
        images_path = os.path.join(SAVE_STATE_PATH, 'images')
        if Path(OLD_SAVE_STATE_PATH).exists():
            print('deleting {} directory...'.format(OLD_SAVE_STATE_PATH))
            # Path(OLD_SAVE_STATE_PATH).rename('.tmp')
            shutil.rmtree(OLD_SAVE_STATE_PATH)
        if Path(SAVE_STATE_PATH).exists():
            print('copying previous {} to {}'.format(SAVE_STATE_PATH, OLD_SAVE_STATE_PATH))
            shutil.copytree(SAVE_STATE_PATH, OLD_SAVE_STATE_PATH)
            shutil.rmtree(SAVE_STATE_PATH)

        Path(images_path).mkdir(parents=True, exist_ok=True)
        f = open('{}/state'.format(SAVE_STATE_PATH), 'wb')
        pickle.dump(self.image_names, f)
        pickle.dump(len(self.roi_options), f)
        f.close()

        # save image surfaces as images
        for image_name, surface in zip(self.image_names, self.image_surfaces):
            # pygame.image.save seems to be a blocking call
            # pygame.image.save(surface.copy(), '{}/{}'.format(images_path, image_name))
            imsave(os.path.join(images_path, image_name), pygame.surfarray.pixels_red(surface.copy()).T)

        # save rois to disk
        for roi_index, roi_option in enumerate(self.roi_options):
            roi_option.save_to_disk(roi_index)

        # send event that the save is complete
        event_data = {'user_type': EVENT_SAVE_STATE_COMPLETE}
        export_images_event = pygame.event.Event(pygame.USEREVENT, event_data)
        pygame.event.post(export_images_event)

    def load_state(self):
        images_path = os.path.join(SAVE_STATE_PATH, 'images')
        print(images_path)
        try:
            f = open(os.path.join(SAVE_STATE_PATH, 'state'), 'rb')
            self.image_names = pickle.load(f)
            self.image_surfaces = []
            self.layer_surfaces = []
            self.roi_options = []
            num_roi = pickle.load(f)
            self.selected_roi_index = None
            self.draw_mode_enabled = False
            print(self.selected_roi_index)
            f.close()

            for image_name in self.image_names:
                surface = pygame.image.load(os.path.join(images_path, image_name))
                self.image_surfaces.append(surface.convert())
            for roi_index in range(num_roi):
                roi_option = RoiOption()
                roi_option.load_from_disk(roi_index)
                self.roi_options.append(roi_option)

            # send event that the save is complete
            event_data = {'user_type': EVENT_LOAD_STATE_COMPLETE}
            export_images_event = pygame.event.Event(pygame.USEREVENT, event_data)
            pygame.event.post(export_images_event)
        except OSError as err:
            # send event that the save is complete
            event_data = {'user_type': EVENT_LOAD_STATE_NOT_FOUND}
            export_images_event = pygame.event.Event(pygame.USEREVENT, event_data)
            pygame.event.post(export_images_event)

    def process_images(self,
                       image_names: List[str],
                       layer_names: List[List[str]] = None,
                       pixel_threshold: int = 100):
        # this is just for demo purposes, requires the files to be in the same directory as manseg.py
        # if image_names is None:
        #     image_names = ['slice{}.png'.format(str(i).zfill(4)) for i in range(10)]

        for image_path in image_names:
            # When generating a pygame.Surface from an ndarray, they are visually seen as the transpose of the
            # original image... therefore we get the transpose of the image so they are shown correctly in the image
            # viewer
            name = os.path.basename(image_path)
            if name not in self.image_names:
                image = imread(image_path).T
                self.image_surfaces.append(pygame.surfarray.make_surface(np.dstack((image, image, image))).convert())
                self.image_names.append(name)
                for roi_option in self.roi_options:
                    roi_option.add_image()

        if layer_names is not None:
            for layer_name in layer_names:
                # first read in the images
                images = [imread(name).T for name in layer_name]
                # then convert each one into a pygame.Surface
                layer = []
                for image in images:
                    image[image >= pixel_threshold] = 255
                    image[image < pixel_threshold] = 0
                    # empty_image = np.zeros(image.shape)
                    surface: pygame.Surface = pygame.surfarray.make_surface(
                        np.dstack((image, image, image))).convert()
                    surface.set_colorkey(TRANS)
                    layer.append(surface)
                self.layer_surfaces.append(layer)
        # send event that the export is complete
        event_data = {'user_type': EVENT_IMPORT_IMAGES_COMPLETE}
        import_images_event = pygame.event.Event(pygame.USEREVENT, event_data)
        pygame.event.post(import_images_event)

    def export_images(self):
        base_path = 'rois'
        if Path(base_path).exists():
            shutil.rmtree(base_path)
        for i, roi_option in enumerate(self.roi_options):
            # roi directories will be in the format of <roi_index>_<x_position>_<y_position>_<roi_x>_<roi_y>
            # example: 0_125_400_800_800
            roi_path = os.path.join(base_path, '{}_{}_{}_{}_{}'.format(i,
                                                                       roi_option.get_position()[0],
                                                                       roi_option.get_position()[1],
                                                                       roi_option.get_size()[0],
                                                                       roi_option.get_size()[1]))

            # exporting the cropped ROI for each image
            image_crops_path = os.path.join(roi_path, 'crops')
            Path(image_crops_path).mkdir(parents=True, exist_ok=True)
            for image_index, image_name in enumerate(self.image_names):
                filename = os.path.basename(image_name)
                name, extension = os.path.splitext(filename)
                surface = self.image_surfaces[image_index].subsurface(
                    pygame.Rect(roi_option.get_position(), roi_option.get_size()))
                imsave(os.path.join(image_crops_path,
                                    '{}_{}_{}_{}_{}{}'.format(name,
                                                              roi_option.get_position()[0],
                                                              roi_option.get_position()[1],
                                                              roi_option.get_size()[0],
                                                              roi_option.get_size()[1],
                                                              extension)),
                       pygame.surfarray.pixels_red(surface).T)

            # exporting each layer
            for layer_index in range(roi_option.get_layer_count()):
                layer_name = roi_option.get_layer_name(layer_index)
                layer_type = roi_option.get_layer_type(layer_index)
                image_path = '{}/layers/{}'.format(roi_path, layer_name)
                Path(image_path).mkdir(parents=True, exist_ok=True)
                color_channel_to_export = LAYER_OPTIONS[layer_type].index(255)
                surface_to_array = None
                if color_channel_to_export == 0:
                    surface_to_array = pygame.surfarray.pixels_red
                elif color_channel_to_export == 1:
                    surface_to_array = pygame.surfarray.pixels_green
                elif color_channel_to_export == 2:
                    surface_to_array = pygame.surfarray.pixels_blue
                for image_index, image_layer in enumerate(roi_option.get_layer(layer_index)):
                    filename = os.path.basename(self.image_names[image_index])
                    image_name, image_extension = os.path.splitext(filename)
                    imsave(os.path.join(image_path,
                                        '{}_{}_{}_{}_{}{}'.format(image_name,
                                                                  roi_option.get_position()[0],
                                                                  roi_option.get_position()[1],
                                                                  roi_option.get_size()[0],
                                                                  roi_option.get_size()[1],
                                                                  image_extension)),
                           surface_to_array(image_layer.copy()).T)

        # send event that the export is complete
        event_data = {'user_type': EVENT_EXPORT_IMAGES_COMPLETE}
        export_images_event = pygame.event.Event(pygame.USEREVENT, event_data)
        pygame.event.post(export_images_event)

    def recreate_ui(self):
        self.ui_manager.set_window_resolution(self.resolution)
        self.ui_manager.clear_and_reset()

        self.background_surface = pygame.Surface(self.resolution)

        self.background_surface.fill(self.ui_manager.ui_theme.get_colour(None, None, 'dark_bg'))

        # load the file dialog if there are no surfaces available
        if len(self.image_surfaces) == 0:
            left_centered = (self.resolution[0]) / 2 - 250
            top_centered = self.resolution[1] / 2 - 250

            self.file_import_dialog = FileImportDialog(rect=pygame.Rect((left_centered, top_centered), (500, 500)),
                                                       manager=self.ui_manager,
                                                       window_title='Load Images',
                                                       # initial_file_path='',
                                                       allow_existing_files_only=False,
                                                       allow_picking_directories=False)
            self.file_import_dialog.cancel_button.disable()
            self.file_import_dialog.close_window_button.disable()
        else:
            # we use the right side of the window to determine the location of the sidebar
            self.sidebar = Sidebar(
                relative_rect=pygame.Rect((self.window_surface.get_rect().right - SIDEBAR_WIDTH, 0),
                                          (SIDEBAR_WIDTH, self.resolution[1])),
                starting_layer_height=4,
                manager=self.ui_manager,
                starting_image=self.image_index,
                num_images=len(self.image_surfaces),
                image_names_list=self.image_names,
                roi_selected_option=self.selected_roi_index,
                roi_options_list=[str(i) for i, option in enumerate(self.roi_options)])
            # when the image roi is less than the minimum resolution, we center it in the view
            left_centered = (self.resolution[0] - SIDEBAR_WIDTH) / 2 - self.roi_resolution[0] / 2
            top_centered = self.resolution[1] / 2 - self.roi_resolution[1] / 2

            if not self.draw_mode_enabled:
                # dialog for setting the roi
                # self.roi_selection_dialog = RoiSelectionDialog(
                #     rect=pygame.Rect(0, 0, 260, 250),
                #     manager=self.ui_manager,
                #     window_title='ROI selection',
                # )
                # self.roi_selection_dialog.set_roi_size(self.roi_resolution)
                # self.roi_selection_dialog.set_roi_position(self.roi_position)
                self.sidebar.roi_selection.set_roi_size(self.roi_resolution)
                self.sidebar.roi_selection.set_roi_position(self.roi_position)

                self.image_explorer = ImageExplorer(
                    relative_rect=pygame.Rect((left_centered, top_centered), self.roi_resolution),
                    image_surface=self.image_surfaces[self.image_index],
                    manager=self.ui_manager)
                self.image_explorer.set_image_position(self.roi_position)
            else:
                roi_option = self.roi_options[self.selected_roi_index]
                self.image_draw = ImageDraw(
                    relative_rect=pygame.Rect((left_centered, top_centered), roi_option.get_size()),
                    background_surface=self.image_surfaces[self.image_index].subsurface(
                        pygame.Rect(roi_option.get_position(), roi_option.get_size())),
                    manager=self.ui_manager,
                    layer_surfaces=roi_option.get_layers_for_image(self.image_index)
                )
                layer_names = roi_option.get_layer_names()
                layer_types = roi_option.get_layer_types()
                self.sidebar.set_roi_labels(roi_option.get_size(), roi_option.get_position())

                # TODO: What layer do we want selected when we cycle through ROIs?
                if len(layer_names) > 0:
                    self.image_draw.set_cursor_color(LAYER_OPTIONS[layer_types[-1]])
                    for name, layer_type in zip(layer_names, layer_types):
                        self.sidebar.handle_add_layer_button(name, layer_type)
                    self.sidebar.enable_alpha_slider(True)

            # height of the preview screen is determined by the component itself, we only need the desired width for
            # scaling
            # the positioning of the preview screen is determined from the bottom of the sidebar
            self.preview_screen = PreviewScreen(
                relative_rect=pygame.Rect((0, self.sidebar.rect.bottom), (SIDEBAR_WIDTH, -1)),
                manager=self.ui_manager,
                container=self.sidebar,
                background=self.image_surfaces[self.image_index],
                roi_size=self.roi_resolution
            )
            self.preview_screen.set_roi_position(self.roi_position)
            # selected_roi_index will only exist after a user has defined an roi
            if self.selected_roi_index is not None:
                self.preview_screen.set_roi_position(self.roi_options[self.selected_roi_index].get_position())

    def set_resolution_from_roi(self, roi_resolution):
        previous_resolution = self.resolution
        suggested_resolution = (roi_resolution[0] + SIDEBAR_WIDTH, roi_resolution[1])
        if previous_resolution[0] < suggested_resolution[0] or previous_resolution[1] < suggested_resolution[1]:
            suggested_resolution = (roi_resolution[0] + SIDEBAR_WIDTH, roi_resolution[1])
            if suggested_resolution[0] < MIN_WINDOW_RESOLUTION[0]:
                suggested_resolution = (MIN_WINDOW_RESOLUTION[0], suggested_resolution[1])
            if suggested_resolution[1] < MIN_WINDOW_RESOLUTION[1]:
                suggested_resolution = (suggested_resolution[0], MIN_WINDOW_RESOLUTION[1])
            self.resolution = suggested_resolution
        # only update the resolution if it changed
        if previous_resolution != self.resolution:
            self.window_surface = pygame.display.set_mode(self.resolution, HWSURFACE | DOUBLEBUF | RESIZABLE)

    def process_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN and event.mod & pygame.KMOD_LCTRL and event.key == pygame.K_s:
                if self.loading_message_window is not None and self.loading_message_window.alive():
                    continue

                print('saving state...')
                self.loading_message_window = LoadingMessageWindow(manager=self.ui_manager,
                                                                   html_message='Saving current state...')
                self.loading_message_window.set_blocking(True)
                self.loading_message_window_thread = Thread(target=self.save_state)
                self.loading_message_window_thread.start()
                # self.save_state()

                # self.debug_mode = not self.debug_mode
                # self.ui_manager.set_visual_debug_mode(self.debug_mode)
            elif event.type == pygame.KEYDOWN and event.mod & pygame.KMOD_LCTRL and event.key == pygame.K_l:
                if self.loading_message_window is not None and self.loading_message_window.alive():
                    continue
                print('loading previous state...')
                self.ui_manager.clear_and_reset()

                self.loading_message_window = LoadingMessageWindow(manager=self.ui_manager,
                                                                   html_message='Loading previous state...')
                self.loading_message_window.set_blocking(True)
                self.loading_message_window_thread = Thread(target=self.load_state)
                self.loading_message_window_thread.start()
                # self.load_state()

            elif event.type == pygame.KEYDOWN and event.mod & pygame.KMOD_LCTRL and event.key == pygame.K_e:
                if self.loading_message_window is not None and self.loading_message_window.alive():
                    continue
                print('exporting images...')
                self.loading_message_window = LoadingMessageWindow(manager=self.ui_manager,
                                                                   html_message='Exporting images...')
                self.loading_message_window.set_blocking(True)
                # self.loading_message_window.
                self.loading_message_window_thread = Thread(target=self.export_images)
                # self.t.daemon = True
                self.loading_message_window_thread.start()
                # self.export_images()
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                if not self.file_import_dialog.alive():
                    left_centered = (self.resolution[0]) / 2 - 250
                    top_centered = self.resolution[1] / 2 - 250
                    self.file_import_dialog = FileImportDialog(
                        rect=pygame.Rect((left_centered, top_centered), (500, 500)),
                        manager=self.ui_manager,
                        window_title='Load Images...',
                        # initial_file_path='',
                        allow_existing_files_only=False,
                        allow_picking_directories=False)
                    self.file_import_dialog.set_blocking(True)

            self.ui_manager.process_events(event)

            # Normally this event would only report once every time the user lets go of the left mouse button when
            # resizing. The current SDL2 implementation seems to cause it to report for every single resize event,
            # which prevents smooth resizing. We debounce the resize event with a 300ms delay as a workaround.
            if event.type == pygame.VIDEORESIZE:
                self.resolution = event.size
                pygame.time.set_timer(pygame.USEREVENT + 1, 300)
            elif event.type == pygame.USEREVENT + 1:
                pygame.time.set_timer(pygame.USEREVENT + 1, 0)
                self.set_resolution_from_roi(self.roi_resolution)
                self.window_surface = pygame.display.set_mode(self.resolution, HWSURFACE | DOUBLEBUF | RESIZABLE)
                self.recreate_ui()

            elif event.type == pygame.USEREVENT:
                if event.user_type == UI_FILE_DIALOG_PATH_PICKED:
                    print(event)
                    try:
                        # self.file_import_dialog = None
                        image_names = event.files
                        print('processing images...')
                        self.loading_message_window = LoadingMessageWindow(manager=self.ui_manager,
                                                                           html_message='Processing images...')
                        self.loading_message_window.set_blocking(True)
                        self.import_thread = Thread(target=self.process_images, args=(image_names,))
                        self.import_thread.start()
                    except pygame.error:
                        pass
                elif event.user_type == EVENT_SAVE_STATE_COMPLETE:
                    self.loading_message_window.kill()
                    self.loading_message_window = LoadingMessageWindow(manager=self.ui_manager,
                                                                       html_message='Current state saved.',
                                                                       can_close=True)
                    self.loading_message_window.set_blocking(True)
                    self.loading_message_window_thread.join()
                    print('save state complete.')
                elif event.user_type == EVENT_LOAD_STATE_COMPLETE:
                    self.recreate_ui()
                    self.loading_message_window.kill()
                    self.loading_message_window = LoadingMessageWindow(manager=self.ui_manager,
                                                                       html_message='Previous state loaded.',
                                                                       can_close=True)
                    self.loading_message_window.set_blocking(True)
                    self.loading_message_window_thread.join()
                    print('load state complete.')
                elif event.user_type == EVENT_LOAD_STATE_NOT_FOUND:
                    self.recreate_ui()
                    self.loading_message_window.kill()
                    self.loading_message_window = LoadingMessageWindow(manager=self.ui_manager,
                                                                       html_message='Previous state not found.',
                                                                       can_close=True)
                    self.loading_message_window.set_blocking(True)
                    print('load state not found.')
                elif event.user_type == EVENT_IMPORT_IMAGES_COMPLETE:
                    print('processing images complete.')
                    if self.loading_message_window is not None:
                        self.loading_message_window.kill()
                    if self.import_thread is not None:
                        self.import_thread.join()
                    self.recreate_ui()
                elif event.user_type == EVENT_EXPORT_IMAGES_COMPLETE:
                    print('exporting images complete.')
                    self.loading_message_window.kill()
                    self.loading_message_window = LoadingMessageWindow(manager=self.ui_manager,
                                                                       html_message='Export completed.',
                                                                       can_close=True)
                    self.loading_message_window.set_blocking(True)
                    self.loading_message_window_thread.join()

                # roi selection dialog events
                elif event.user_type == EVENT_ROI_SELECTION_PREVIEW:
                    self.roi_resolution = event.size
                    if event.position is not None:
                        self.roi_position = event.position
                    self.set_resolution_from_roi(self.roi_resolution)
                    self.recreate_ui()
                elif event.user_type == EVENT_ROI_SELECTION_ADD:
                    roi_option = RoiOption(
                        size=event.size,
                        position=event.position,
                        num_images=len(self.image_surfaces),
                    )
                    # TODO: add any existing layers to the RoiOption
                    for layer in self.layer_surfaces:
                        roi_option.add_layer(name='undefined',
                                             layer_type='undefined',
                                             layer_surface=[
                                                 surface.subsurface(
                                                     pygame.Rect(roi_option.position, roi_option.size)).copy() for
                                                 surface in
                                                 layer])
                    self.roi_options.append(roi_option)

                    # once the newly added roi_option is set, we select the last one in the list
                    self.selected_roi_index = len(self.roi_options) - 1
                    self.roi_resolution = event.size
                    self.set_resolution_from_roi(self.roi_resolution)
                    self.draw_mode_enabled = True
                    self.recreate_ui()
                elif event.user_type == EVENT_ROI_SELECTION_CANCEL:
                    if len(self.roi_options) > 0:
                        self.selected_roi_index = len(self.roi_options) - 1
                        self.roi_resolution = self.roi_options[self.selected_roi_index].get_size()
                        self.set_resolution_from_roi(self.roi_resolution)
                        self.draw_mode_enabled = True
                    self.recreate_ui()

                # layer dialog events
                elif event.user_type == EVENT_LAYER_DIALOG_CREATE:
                    # generate an empty layer for each available image and add it to the overlays
                    roi_option = self.roi_options[self.selected_roi_index]
                    roi_option.add_layer(event.layer_name, event.layer_type)
                    if self.image_draw is not None:
                        self.image_draw.set_layer_surfaces(roi_option.get_layers_for_image(self.image_index))
                        self.image_draw.set_draw_surface(-1)
                        self.image_draw.set_cursor_color(LAYER_OPTIONS[roi_option.get_layer_type(-1)])
                    self.sidebar.handle_add_layer_button(event.layer_name, event.layer_type)
                    self.sidebar.enable_alpha_slider(True)
                elif event.user_type == EVENT_LAYER_DIALOG_UPDATE:
                    roi_option = self.roi_options[self.selected_roi_index]
                    roi_option.set_layer_name(event.layer_index, event.layer_name)
                    prev_layer_type = roi_option.get_layer_type(event.layer_index)
                    roi_option.set_layer_type(event.layer_index, event.layer_type)
                    if prev_layer_type != event.layer_type:
                        layer = roi_option.get_layer(event.layer_index)
                        for surface in layer:
                            pygame.transform.threshold(
                                dest_surf=surface,
                                surf=surface,
                                search_color=LAYER_OPTIONS[prev_layer_type],
                                threshold=(0, 0, 0),
                                set_color=LAYER_OPTIONS[event.layer_type],
                                set_behavior=1,
                                search_surf=None,
                                inverse_set=True
                            )
                        self.image_draw.set_cursor_color(LAYER_OPTIONS[event.layer_type])
                        self.image_draw.draw()
                    # self.sidebar.layer_selection_buttons[event.layer_index].set_text(event.layer_name)
                    self.sidebar.handle_update_layer_button(event.layer_index, event.layer_name, event.layer_type)
                # image explorer events
                elif event.user_type == EVENT_IMAGE_EXPLORER_PAN:
                    self.roi_position = event.value
                    self.preview_screen.set_roi_position(self.roi_position)
                    # self.roi_selection_dialog.set_roi_position(self.roi_position)
                    self.sidebar.roi_selection.set_roi_position(self.roi_position)

                # image draw events
                elif event.user_type == EVENT_IMAGE_DRAW_BRUSH_UP:
                    self.sidebar.set_brush_size(event.value)
                elif event.user_type == EVENT_IMAGE_DRAW_BRUSH_DOWN:
                    self.sidebar.set_brush_size(event.value)

                # sidebar events
                elif event.user_type == EVENT_SIDEBAR_BUTTON_ADD_ROI:
                    self.roi_resolution = DEFAULT_ROI_RESOLUTION
                    self.set_resolution_from_roi(self.roi_resolution)
                    self.selected_roi_index = None
                    # TODO: should we reset the roi resolution and position to the default, or keep the previous?
                    # self.roi_resolution = DEFAULT_ROI_RESOLUTION
                    # self.roi_position = DEFAULT_ROI_POSITION
                    self.draw_mode_enabled = False
                    self.recreate_ui()
                elif event.user_type == EVENT_SIDEBAR_BUTTON_REMOVE_ROI:
                    # remove selected_roi
                    self.roi_options.pop(self.selected_roi_index)
                    # if list is empty, reset ui to image explorer
                    if len(self.roi_options) == 0:
                        self.selected_roi_index = None
                        self.draw_mode_enabled = False
                        self.roi_resolution = DEFAULT_ROI_RESOLUTION
                    # if list is not empty, check if removed roi was the first one
                    # if not the first one, then subtract 1
                    elif self.selected_roi_index > 0:
                        self.selected_roi_index -= 1
                        self.roi_resolution = self.roi_options[self.selected_roi_index].get_size()
                    elif self.selected_roi_index == 0:
                        self.roi_resolution = self.roi_options[self.selected_roi_index].get_size()
                    self.set_resolution_from_roi(self.roi_resolution)
                    self.recreate_ui()
                elif event.user_type == EVENT_SIDEBAR_SELECT_ROI:
                    previous_roi_index = self.selected_roi_index
                    if previous_roi_index != event.value:
                        self.selected_roi_index = event.value
                        self.roi_resolution = self.roi_options[self.selected_roi_index].get_size()
                        self.set_resolution_from_roi(self.roi_resolution)

                        self.draw_mode_enabled = True
                        self.recreate_ui()
                elif event.user_type == EVENT_SIDEBAR_IMAGE_SLIDER_MOVED:
                    self.image_index = event.value
                    selected_image = self.image_surfaces[self.image_index]
                    if self.draw_mode_enabled:
                        if self.image_draw is not None:
                            position = self.roi_options[self.selected_roi_index].get_position()
                            size = self.roi_options[self.selected_roi_index].get_size()
                            self.image_draw.set_surfaces(
                                self.image_surfaces[self.image_index].subsurface(pygame.Rect(position, size)),
                                self.roi_options[self.selected_roi_index].get_layers_for_image(self.image_index))
                    elif self.image_explorer is not None:
                        self.image_explorer.set_image_surface(selected_image)

                    if self.preview_screen is not None:
                        self.preview_screen.set_background(selected_image)
                        # self.preview_screen.set_overlays(selected_overlays)
                elif event.user_type == EVENT_SIDEBAR_BRUSH_SLIDER_MOVED:
                    if self.image_draw is not None:
                        self.image_draw.set_brush_size(event.value)
                elif event.user_type == EVENT_SIDEBAR_BUTTON_DRAW_BUTTON:
                    if self.image_draw is not None:
                        self.image_draw.mouse_button_left_handler = self.image_draw.handle_draw
                elif event.user_type == EVENT_SIDEBAR_BUTTON_ERASE_BUTTON:
                    if self.image_draw is not None:
                        self.image_draw.mouse_button_left_handler = self.image_draw.handle_erase
                elif event.user_type == EVENT_SIDEBAR_ALPHA_SLIDER_MOVED:
                    layer = self.roi_options[self.selected_roi_index].get_layer(event.layer_index)
                    for surface in layer:
                        surface.set_alpha(event.alpha)
                    self.image_draw.draw()
                elif event.user_type == EVENT_SIDEBAR_BUTTON_ADD_LAYER:
                    LayerDialog(
                        rect=pygame.Rect(0, 0, 260, 225),
                        manager=self.ui_manager,
                        window_title='Add New Layer')
                elif event.user_type == EVENT_SIDEBAR_BUTTON_REMOVE_LAYER:
                    roi_option = self.roi_options[self.selected_roi_index]
                    roi_option.remove_layer(event.layer_index)
                    if roi_option.get_layer_count() == 0:
                        self.sidebar.enable_alpha_slider(False)
                    if self.image_draw is not None:
                        self.image_draw.set_draw_surface(None)
                        self.image_draw.set_layer_surfaces(roi_option.get_layers_for_image(self.image_index))
                        self.image_draw.draw()
                elif event.user_type == EVENT_SIDEBAR_BUTTON_EDIT_LAYER:
                    roi_option = self.roi_options[self.selected_roi_index]
                    LayerDialog(
                        rect=pygame.Rect(0, 0, 260, 225),
                        manager=self.ui_manager,
                        window_title='Edit Layer',
                        layer_name=roi_option.get_layer_name(event.layer_index),
                        layer_type=roi_option.get_layer_type(event.layer_index),
                        layer_index=event.layer_index,
                        edit_mode=True)
                elif event.user_type == EVENT_SIDEBAR_SELECT_LAYER:
                    if self.image_draw is not None:
                        roi_option = self.roi_options[self.selected_roi_index]
                        layer = roi_option.get_layer(event.layer_index)
                        layer_alpha = layer[0].get_alpha()
                        if layer_alpha is None:
                            layer_alpha = 255
                        self.sidebar.set_alpha_slider_value(layer_alpha)

                        self.image_draw.set_draw_surface(event.layer_index)
                        self.image_draw.set_cursor_color(
                            LAYER_OPTIONS[roi_option.get_layer_type(event.layer_index)])

    def run(self):
        while self.running:
            time_delta = self.clock.tick(60) / 1000.0

            # check for input
            self.process_events()

            # respond to user input
            self.ui_manager.update(time_delta)

            # draw graphics
            self.window_surface.blit(self.background_surface, (0, 0))
            self.ui_manager.draw_ui(self.window_surface)

            pygame.display.update()


if __name__ == '__main__':
    app = MansegApp()

    # processing canned set of images
    # app.process_images()

    # initial render of the UI
    app.recreate_ui()

    # main event loop
    app.run()
