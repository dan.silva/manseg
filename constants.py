MIN_WINDOW_RESOLUTION = (850, 600)
DEFAULT_ROI_RESOLUTION = (800, 800)
DEFAULT_ROI_POSITION = (0, 0)
SIDEBAR_WIDTH = 300
DEFAULT_WINDOW_RESOLUTION = (DEFAULT_ROI_RESOLUTION[0] + SIDEBAR_WIDTH, DEFAULT_ROI_RESOLUTION[1])

COMPONENT_PADDING = 10
COMPONENT_HEIGHT = 25
MIN_COMPONENT_WIDTH = 25

BRUSH_SIZE_DEFAULT = 2
BRUSH_SIZE_MIN = 1
BRUSH_SIZE_MAX = 20

ALPHA_MIN = 0
ALPHA_MAX = 255
ALPHA_DEFAULT = 255

LAYER_OPTIONS = {
    'undefined': (255, 255, 255),
    'membranes': (0, 255, 0),
    'mitochondria': (255, 0, 0),
    'nuclei': (0, 0, 255),
    'vesicles': (255, 255, 0)
}

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY = (200, 200, 200)
ORANGE = (255, 204, 153)
CYAN = (0, 255, 255)
MAGENTA = (255, 0, 255)
TRANS = (0, 0, 0, 0)

# event constants
EVENT_IMAGE_EXPLORER_PAN = 'event_image_explorer_pan'

EVENT_IMAGE_DRAW_BRUSH_UP = 'event_image_draw_brush_up'
EVENT_IMAGE_DRAW_BRUSH_DOWN = 'event_image_draw_brush_down'
EVENT_IMAGE_DRAW_BRUSH_DRAW = 'event_image_draw_brush_draw'
EVENT_IMAGE_DRAW_BRUSH_ERASE = 'event_image_draw_brush_erase'

EVENT_SIDEBAR_BUTTON_ADD_ROI = 'event_sidebar_button_add_roi'
EVENT_SIDEBAR_BUTTON_REMOVE_ROI = 'event_sidebar_button_remove_roi'
EVENT_SIDEBAR_SELECT_ROI = 'event_sidebar_select_roi'
EVENT_SIDEBAR_ALPHA_SLIDER_MOVED = 'event_sidebar_alpha_slider_moved'
EVENT_SIDEBAR_IMAGE_SLIDER_MOVED = 'event_sidebar_image_slider_moved'
EVENT_SIDEBAR_BRUSH_SLIDER_MOVED = 'event_sidebar_brush_slider_moved'
EVENT_SIDEBAR_BUTTON_ADD_LAYER = 'event_sidebar_button_add_layer'
EVENT_SIDEBAR_BUTTON_REMOVE_LAYER = 'event_sidebar_button_remove_layer'
EVENT_SIDEBAR_BUTTON_EDIT_LAYER = 'event_sidebar_button_edit_layer'
EVENT_SIDEBAR_BUTTON_DRAW_BUTTON = 'event_sidebar_button_draw_button'
EVENT_SIDEBAR_BUTTON_ERASE_BUTTON = 'event_sidebar_button_erase_button'
EVENT_SIDEBAR_SELECT_LAYER = 'event_sidebar_select_layer'

EVENT_ROI_SELECTION_PREVIEW = 'event_roi_selection_preview'
EVENT_ROI_SELECTION_ADD = 'event_roi_selection_add'
EVENT_ROI_SELECTION_CANCEL = 'event_roi_selection_cancel'

EVENT_LAYER_DIALOG_CREATE = 'event_layer_dialog_create'
EVENT_LAYER_DIALOG_UPDATE = 'event_layer_dialog_update'

EVENT_EXPORT_IMAGES_COMPLETE = 'event_export_images_complete'
EVENT_IMPORT_IMAGES_COMPLETE = 'event_import_images_complete'

EVENT_SAVE_STATE_COMPLETE = 'event_save_state_complete'
EVENT_LOAD_STATE_COMPLETE = 'event_load_state_complete'
EVENT_LOAD_STATE_NOT_FOUND = 'event_load_state_not_found'

SAVE_STATE_PATH = '.save_state'
OLD_SAVE_STATE_PATH = '.old_save_state'
SAVE_STATE_ROI_PATH = SAVE_STATE_PATH + '/rois'
