import warnings
from typing import Tuple, Union, Dict, List, Optional

import numpy as np
import pygame
from pygame_gui import UI_BUTTON_PRESSED, UI_DROP_DOWN_MENU_CHANGED, \
    UI_TEXT_ENTRY_CHANGED, UI_HORIZONTAL_SLIDER_MOVED, UI_BUTTON_DOUBLE_CLICKED, UI_FILE_DIALOG_PATH_PICKED
from pygame_gui.core import UIElement, IContainerLikeInterface, UIContainer
from pygame_gui.core.interfaces import IUIManagerInterface, IUIContainerInterface
from pygame_gui.elements import UIWindow, UIButton, UIDropDownMenu, UITextEntryLine, UILabel, UIPanel, \
    UIHorizontalSlider, UIScrollingContainer, UIImage, UISelectionList
from pygame_gui.windows import UIMessageWindow, UIFileDialog

from constants import EVENT_IMAGE_EXPLORER_PAN, EVENT_IMAGE_DRAW_BRUSH_UP, EVENT_IMAGE_DRAW_BRUSH_DOWN, \
    EVENT_IMAGE_DRAW_BRUSH_DRAW, \
    EVENT_IMAGE_DRAW_BRUSH_ERASE, EVENT_ROI_SELECTION_PREVIEW, EVENT_ROI_SELECTION_ADD, EVENT_SIDEBAR_BUTTON_ADD_ROI, \
    EVENT_SIDEBAR_SELECT_ROI, EVENT_SIDEBAR_IMAGE_SLIDER_MOVED, EVENT_SIDEBAR_BRUSH_SLIDER_MOVED, BRUSH_SIZE_DEFAULT, \
    BRUSH_SIZE_MAX, BRUSH_SIZE_MIN, TRANS, EVENT_SIDEBAR_BUTTON_REMOVE_ROI, EVENT_SIDEBAR_BUTTON_DRAW_BUTTON, \
    EVENT_SIDEBAR_BUTTON_ERASE_BUTTON, COMPONENT_PADDING, EVENT_SIDEBAR_BUTTON_ADD_LAYER, \
    EVENT_SIDEBAR_BUTTON_REMOVE_LAYER, EVENT_LAYER_DIALOG_CREATE, LAYER_OPTIONS, EVENT_SIDEBAR_SELECT_LAYER, \
    EVENT_SIDEBAR_BUTTON_EDIT_LAYER, EVENT_LAYER_DIALOG_UPDATE, EVENT_ROI_SELECTION_CANCEL, COMPONENT_HEIGHT, \
    MIN_COMPONENT_WIDTH, EVENT_SIDEBAR_ALPHA_SLIDER_MOVED, ALPHA_DEFAULT, ALPHA_MIN, ALPHA_MAX


class ImageExplorer(UIElement):
    def __init__(self,
                 relative_rect: pygame.Rect,
                 image_surface: pygame.Surface,
                 manager: IUIManagerInterface,
                 container: Union[IContainerLikeInterface, None] = None,
                 parent_element: UIElement = None,
                 object_id: Union[str, None] = None,
                 anchors: Dict[str, str] = None
                 ):
        new_element_ids, new_object_ids = self._create_valid_ids(container=container,
                                                                 parent_element=parent_element,
                                                                 object_id=object_id,
                                                                 element_id='image_explorer')

        super().__init__(relative_rect, manager, container,
                         starting_height=1,
                         layer_thickness=1,
                         object_ids=new_object_ids,
                         element_ids=new_element_ids,
                         anchors=anchors)

        self.is_pan = False
        self.current_image_position = (0, 0)
        self.prev_mouse_position = (0, 0)
        self.image_surface = image_surface
        self.draw()

    def set_image_surface(self, image_surface: pygame.Surface):
        self.image_surface = image_surface
        self.draw()

    def handle_pan(self, mouse_position):
        self.is_pan = True
        self.prev_mouse_position = mouse_position

    def handle_mouse_move(self, mouse_position):
        pos_diff = np.subtract(self.prev_mouse_position, mouse_position)
        temp_position = tuple(np.add(self.current_image_position, pos_diff))
        # left x boundary
        if temp_position[0] < 0:
            temp_position = (0, temp_position[1])
        # right x boundary
        if np.abs(temp_position[0]) + self.rect.width > self.image_surface.get_width():
            temp_position = ((self.image_surface.get_width() - self.rect.width), temp_position[1])
        # # upper y boundary
        if temp_position[1] < 0:
            temp_position = (temp_position[0], 0)
        # lower y boundary
        if np.abs(temp_position[1]) + self.rect.height > self.image_surface.get_height():
            temp_position = (temp_position[0], (self.image_surface.get_height() - self.rect.height))
        self.current_image_position = temp_position

        self.prev_mouse_position = mouse_position

    def set_dimensions(self, dimensions: Union[pygame.math.Vector2,
                                               Tuple[int, int],
                                               Tuple[float, float]]):
        super().set_dimensions(dimensions)
        self.draw()

    def get_image_position(self):
        return self.current_image_position

    def set_image_position(self, position: Tuple[int, int]):
        self.current_image_position = position
        self.draw()

    def process_event(self, event: pygame.event.Event) -> bool:
        consumed_event = False
        scaled_mouse_pos = None
        if self is not None:
            # if the mouse leaves the window, stop panning
            if event.type == pygame.ACTIVEEVENT:
                if event.gain == 0:
                    self.is_pan = False
            if event.type == pygame.MOUSEBUTTONUP and event.button == pygame.BUTTON_LEFT:
                self.is_pan = False
            if (event.type == pygame.MOUSEBUTTONDOWN and
                    event.button in [
                        pygame.BUTTON_LEFT,
                        # pygame.BUTTON_RIGHT,
                        # pygame.BUTTON_MIDDLE
                    ]):
                scaled_mouse_pos = (int(event.pos[0] * self.ui_manager.mouse_pos_scale_factor[0]),
                                    int(event.pos[1] * self.ui_manager.mouse_pos_scale_factor[1]))
                mouse_pos = (scaled_mouse_pos[0] - self.rect.left, scaled_mouse_pos[1] - self.rect.top)
                self.handle_pan(mouse_pos)

            if event.type == pygame.MOUSEMOTION and self.is_pan:
                scaled_mouse_pos = (int(event.pos[0] * self.ui_manager.mouse_pos_scale_factor[0]),
                                    int(event.pos[1] * self.ui_manager.mouse_pos_scale_factor[1]))
                mouse_pos = (scaled_mouse_pos[0] - self.rect.left, scaled_mouse_pos[1] - self.rect.top)
                self.handle_mouse_move(mouse_pos)

                # if scaled_mouse_pos is not None and self.hover_point(scaled_mouse_pos[0], scaled_mouse_pos[1]):
                self.draw()
                # generate pan event
                event_data = {
                    'user_type': EVENT_IMAGE_EXPLORER_PAN,
                    'value': self.current_image_position
                }

                pygame.event.post(pygame.event.Event(pygame.USEREVENT, event_data))
                consumed_event = True

        return consumed_event

    def draw(self):
        # create new empty surface
        roi_surface = pygame.Surface(self.rect.size)
        # draw the roi to be displayed
        roi_surface.blit(self.image_surface, (0, 0),
                         (self.current_image_position[0], self.current_image_position[1], self.rect.width,
                          self.rect.height))

        self.set_image(roi_surface)


class ImageDraw(UIElement):
    def __init__(self,
                 relative_rect: pygame.Rect,
                 background_surface: pygame.Surface,
                 layer_surfaces: List[pygame.Surface],
                 manager: IUIManagerInterface,
                 container: Union[IContainerLikeInterface, None] = None,
                 parent_element: UIElement = None,
                 object_id: Union[str, None] = None,
                 anchors: Dict[str, str] = None
                 ):

        new_element_ids, new_object_ids = self._create_valid_ids(container=container,
                                                                 parent_element=parent_element,
                                                                 object_id=object_id,
                                                                 element_id='image_draw')

        super().__init__(relative_rect, manager, container,
                         starting_height=1,
                         layer_thickness=1,
                         object_ids=new_object_ids,
                         element_ids=new_element_ids,
                         anchors=anchors)

        # black surface used for setting the background to black
        self.black_background_surface = pygame.Surface(self.rect.size)

        # used to switch between the background_surface and black surface
        self.original_surface = background_surface

        self.background_surface = background_surface

        self.layer_surfaces = layer_surfaces

        # surface used for drawing
        self.draw_surface = pygame.Surface(self.rect.size)

        # surface used for representing the cursor location
        self.cursor_surface = pygame.Surface(self.rect.size, pygame.SRCALPHA)

        # used for swapping out the left button handler
        self.mouse_button_left_handler = self.handle_draw

        self.selected_layer: Optional[pygame.Surface] = None
        self.selected_layer_index: Optional[int] = None
        self.is_mouse_on_screen = False
        self.draw_on = False
        self.background_on = True

        # used for drawing continuous lines
        self.last_mouse_pos = (0, 0)

        # brush colors
        self.color: Tuple[int, int, int] = LAYER_OPTIONS['undefined']
        self.eraser: Tuple[int, int, int] = TRANS
        self.draw_color: Tuple[int, int, int] = self.color

        self.brush_size = BRUSH_SIZE_DEFAULT

        # setting the first overlay surface as the draw surface if it exists
        if len(self.layer_surfaces) > 0:
            self.set_draw_surface(0)
        self.draw()

    def set_surfaces(self, background_surface: pygame.Surface, overlay_surfaces: List[pygame.Surface]):
        self.original_surface = background_surface
        self.background_surface = background_surface
        self.set_layer_surfaces(overlay_surfaces)
        self.black_background_surface = pygame.Surface(
            (self.background_surface.get_width(), self.background_surface.get_height()))
        # selected_overlay_index = 0 if len(overlay_surfaces) > 0 else None
        self.set_draw_surface(self.selected_layer_index)
        self.draw()

    def set_layer_surfaces(self, overlay_surfaces: List[pygame.Surface]):
        self.layer_surfaces = overlay_surfaces

    def set_draw_surface(self, index: int):
        self.selected_layer_index = index
        if self.selected_layer_index is None:
            self.selected_layer = None
        else:
            self.selected_layer = self.layer_surfaces[index]

    def draw(self, position=(0, 0)):
        # self.draw_surface = pygame.Surface(self.rect.size)
        # pygame.Rect((position[0] - 50, position[1] - 50), (100,100))
        area = None
        left, top = position
        # Only draw on a subsection of the image when a position is given, this should only happen when the mouse is
        # moving across the draw screen.
        # Instead of blitting the entire surface multiple times, we are essentially just blitting a 100x100 px square
        if left > 0 or top > 0:
            blit_size = 100
            # we offset this because we want to draw a square with an origin based on the provided position
            area = pygame.Rect((left - (blit_size / 2), top - (blit_size / 2)), (blit_size, blit_size))
            # we use the offset position based on the area we calculated
            position = area.topleft
        self.draw_surface.blit(self.background_surface, position, area)
        for surface in self.layer_surfaces:
            self.draw_surface.blit(surface, position, area)
        self.draw_surface.blit(self.cursor_surface, position, area)
        self.cursor_surface.fill((0, 0, 0, 0), area)
        self.set_image(self.draw_surface)

    def set_cursor_color(self, color: Tuple[int, int, int]):
        self.color = color

    def round_line(self,
                   surface: pygame.Surface,
                   color: Tuple[int, int, int],
                   start: Tuple[int, int],
                   end: Tuple[int, int], radius=1):
        dx = end[0] - start[0]
        dy = end[1] - start[1]
        distance = max(abs(dx), abs(dy))
        for i in range(distance):
            x = int(start[0] + float(i) / distance * dx)
            y = int(start[1] + float(i) / distance * dy)
            pygame.draw.circle(surface, color, (x, y), radius)

    def draw_cursor(self, mouse_position: Tuple[int, int]):
        if self.selected_layer is not None:
            pygame.draw.circle(self.cursor_surface, (self.color[0], self.color[1], self.color[2], 100),
                               mouse_position, self.brush_size)

    def handle_draw(self, mouse_position: Tuple[int, int]):
        if self.selected_layer is not None:
            self.draw_on = True
            self.draw_color = self.color
            self.last_mouse_pos = mouse_position
            pygame.draw.circle(self.selected_layer, self.draw_color, mouse_position, self.brush_size)
            # self.draw()

    def handle_erase(self, mouse_position: Tuple[int, int]):
        if self.selected_layer is not None:
            self.draw_on = True
            self.draw_color = self.eraser
            pygame.draw.circle(self.selected_layer, self.draw_color, mouse_position, self.brush_size)
            # self.draw()

    def handle_brush_up(self, mouse_position: Tuple[int, int]):
        if self.brush_size < BRUSH_SIZE_MAX and self.brush_size:
            self.brush_size += 1
        self.draw_cursor(mouse_position)

    def handle_brush_down(self, mouse_position: Tuple[int, int]):
        if self.brush_size > BRUSH_SIZE_MIN:
            self.brush_size -= 1
        self.draw_cursor(mouse_position)

    def handle_mouse_move(self, mouse_position: Tuple[int, int]):
        if self.draw_on:
            pygame.draw.circle(self.selected_layer, self.draw_color, mouse_position, self.brush_size)
            self.round_line(self.selected_layer, self.draw_color, mouse_position, self.last_mouse_pos,
                            self.brush_size)

        self.last_mouse_pos = mouse_position
        self.draw_cursor(mouse_position)

    def handle_mouse_button_up(self, mouse_position: Tuple[int, int]):
        self.disable_tools()
        self.draw_cursor(mouse_position)

    def handle_background_toggle(self):
        self.background_on = not self.background_on
        if self.background_on:
            self.background_surface = self.original_surface
        else:
            self.background_surface = self.black_background_surface

    def disable_tools(self):
        self.draw_on = False

    def get_brush_size(self):
        return self.brush_size

    def set_brush_size(self, size):
        self.brush_size = size

    def process_event(self, event: pygame.event.Event) -> bool:
        consumed_event = False
        if self is not None:
            if event.type == pygame.MOUSEBUTTONUP and event.button in [pygame.BUTTON_LEFT, pygame.BUTTON_RIGHT]:
                self.draw_on = False
            if event.type in [pygame.MOUSEBUTTONDOWN, pygame.MOUSEMOTION]:
                scaled_mouse_pos = (int(event.pos[0] * self.ui_manager.mouse_pos_scale_factor[0]),
                                    int(event.pos[1] * self.ui_manager.mouse_pos_scale_factor[1]))
                mouse_pos = (scaled_mouse_pos[0] - self.rect.left, scaled_mouse_pos[1] - self.rect.top)
                if self.hover_point(scaled_mouse_pos[0], scaled_mouse_pos[1]):
                    self.is_mouse_on_screen = True
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        event_data = None
                        # TODO: does it make sense to have to separate events for increasing and decreasing the brush size?
                        if event.button == pygame.BUTTON_WHEELUP:
                            self.handle_brush_up(mouse_pos)
                            event_data = {
                                'user_type': EVENT_IMAGE_DRAW_BRUSH_UP,
                                'value': self.brush_size
                            }
                        elif event.button == pygame.BUTTON_WHEELDOWN:
                            self.handle_brush_down(mouse_pos)
                            event_data = {
                                'user_type': EVENT_IMAGE_DRAW_BRUSH_DOWN,
                                'value': self.brush_size
                            }
                        elif event.button == pygame.BUTTON_LEFT:
                            self.mouse_button_left_handler(mouse_pos)
                            event_data = {
                                'user_type': EVENT_IMAGE_DRAW_BRUSH_DRAW
                            }
                        elif event.button == pygame.BUTTON_RIGHT:
                            self.handle_erase(mouse_pos)
                            event_data = {
                                'user_type': EVENT_IMAGE_DRAW_BRUSH_ERASE
                            }
                        if event_data is not None:
                            pygame.event.post(pygame.event.Event(pygame.USEREVENT, event_data))
                    elif event.type == pygame.MOUSEMOTION:
                        # mouse_pos = (scaled_mouse_pos[0] - self.rect.left, scaled_mouse_pos[1] - self.rect.top)
                        self.handle_mouse_move(mouse_pos)
                    consumed_event = True
                    self.draw(mouse_pos)
                else:
                    # draw once more after the mouse leaves the screen
                    if self.is_mouse_on_screen:
                        self.is_mouse_on_screen = False
                        self.draw()
                    pass

            if event.type == pygame.ACTIVEEVENT:
                self.draw()

        return consumed_event


class Sidebar(UIPanel):
    def __init__(self,
                 relative_rect: pygame.Rect,
                 starting_layer_height: int,
                 manager: IUIManagerInterface,
                 starting_image: int,
                 num_images: int,
                 image_names_list: List[str] = [''],
                 roi_selected_option: int = 0,
                 roi_options_list: List[str] = ['']):
        super().__init__(relative_rect,
                         starting_layer_height,
                         manager)
        self.image_controls_container = UIContainer(
            relative_rect=pygame.Rect(
                (COMPONENT_PADDING, COMPONENT_PADDING),
                (relative_rect.width - COMPONENT_PADDING * 2, 100)),
            manager=self.ui_manager,
            starting_height=3,
            container=self)
        self.image_names_drop_down = UIDropDownMenu(
            relative_rect=pygame.Rect(
                (0, 0),
                (self.rect.width - COMPONENT_PADDING * 2, COMPONENT_HEIGHT)),
            manager=self.ui_manager,
            starting_option=image_names_list[starting_image],
            options_list=image_names_list,
            container=self.image_controls_container)
        self.image_slider_title, self.image_slider, self.image_slider_label = self.generate_slider(
            'image:',
            pygame.Rect(self.image_names_drop_down.relative_rect.bottomleft,
                        (self.image_controls_container.rect.width, COMPONENT_HEIGHT)),
            self.image_controls_container,
            start_value=starting_image + 1,
            value_range=(1, num_images))

        self.selected_layer_index = None
        self.roi_selector = None
        self.add_roi_button = None
        self.remove_roi_button = None
        self.brush_slider = None
        self.brush_draw_button = None
        self.brush_erase_button = None
        self.brush_add_layer_button = None
        self.add_layer_button = None
        self.layer_scrolling_container = None
        self.layer_button_containers = []
        self.layer_selection_buttons: List[UIButton] = []
        self.layer_remove_buttons = []
        self.layer_button_colors: List[UIImage] = []

        if roi_selected_option is None:
            self.roi_selection = RoiSelection(
                relative_rect=pygame.Rect((COMPONENT_PADDING, COMPONENT_HEIGHT * 2 + COMPONENT_PADDING * 2),
                                          (self.rect.width, 300)),
                manager=self.ui_manager,
                container=self)
            pass
        else:
            self.roi_selector_label = UILabel(
                relative_rect=pygame.Rect(
                    (COMPONENT_PADDING, COMPONENT_HEIGHT * 2 + COMPONENT_PADDING * 2),
                    (50, COMPONENT_HEIGHT)),
                manager=self.ui_manager,
                container=self,
                text='ROI:'
            )
            self.roi_selector = UIDropDownMenu(
                relative_rect=pygame.Rect(
                    self.roi_selector_label.relative_rect.topright,
                    (
                        self.rect.width - MIN_COMPONENT_WIDTH * 2 - self.roi_selector_label.relative_rect.width - COMPONENT_PADDING * 2,
                        COMPONENT_HEIGHT)),
                manager=self.ui_manager,
                starting_option=roi_options_list[roi_selected_option],
                options_list=roi_options_list,
                container=self,
                anchors={'left': 'left',
                         'right': 'right',
                         'top': 'top',
                         'bottom': 'bottom'})
            self.remove_roi_button = UIButton(
                relative_rect=pygame.Rect(
                    self.roi_selector.relative_rect.topright,
                    (COMPONENT_HEIGHT, COMPONENT_HEIGHT)),
                manager=self.ui_manager,
                container=self,
                text='-')
            self.add_roi_button = UIButton(
                relative_rect=pygame.Rect(
                    self.remove_roi_button.relative_rect.topright,
                    (COMPONENT_HEIGHT, COMPONENT_HEIGHT)),
                manager=self.ui_manager,
                container=self,
                text='+')

            # ROI info
            self.roi_label_width = UILabel(
                relative_rect=pygame.Rect(self.roi_selector_label.relative_rect.bottomleft, (60, COMPONENT_HEIGHT)),
                text='width:',
                manager=self.ui_manager,
                container=self
            )
            self.roi_text_width = UILabel(
                relative_rect=pygame.Rect(self.roi_label_width.relative_rect.topright,
                                          (self.rect.width - COMPONENT_PADDING - self.roi_label_width.rect.width,
                                           COMPONENT_HEIGHT)),
                text='placeholder',
                manager=self.ui_manager,
                container=self
            )
            # self.roi_text_width = UITextEntryLine(
            #     relative_rect=pygame.Rect(self.roi_label_width.relative_rect.topright,
            #                               (self.rect.width - COMPONENT_PADDING - self.roi_label_width.rect.width,
            #                                COMPONENT_HEIGHT)),
            #     manager=self.ui_manager,
            #     container=self
            # )
            self.roi_label_height = UILabel(
                relative_rect=pygame.Rect(self.roi_label_width.relative_rect.bottomleft, (60, COMPONENT_HEIGHT)),
                text='height:',
                manager=self.ui_manager,
                container=self
            )
            self.roi_text_height = UILabel(
                relative_rect=pygame.Rect(self.roi_label_height.relative_rect.topright,
                                          (self.rect.width - COMPONENT_PADDING - self.roi_label_height.rect.width,
                                           COMPONENT_HEIGHT)),
                text='placeholder',
                manager=self.ui_manager,
                container=self
            )
            # self.roi_text_height = UITextEntryLine(
            #     relative_rect=pygame.Rect(self.roi_label_height.relative_rect.topright,
            #                               (text_width - self.roi_label_height.rect.width, -1)),
            #     manager=self.ui_manager,
            #     container=self
            # )

            self.roi_label_x_position = UILabel(
                relative_rect=pygame.Rect(self.roi_label_height.relative_rect.bottomleft, (60, COMPONENT_HEIGHT)),
                text='x:',
                manager=self.ui_manager,
                container=self
            )
            self.roi_text_x_position = UILabel(
                relative_rect=pygame.Rect(self.roi_label_x_position.relative_rect.topright,
                                          (self.rect.width - COMPONENT_PADDING - self.roi_label_x_position.rect.width,
                                           COMPONENT_HEIGHT)),
                text='placeholder',
                manager=self.ui_manager,
                container=self
            )
            # self.roi_text_x_position = UITextEntryLine(
            #     relative_rect=pygame.Rect(self.roi_label_x_position.relative_rect.topright,
            #                               (text_width - self.roi_label_x_position.rect.width, -1)),
            #     manager=self.ui_manager,
            #     container=self
            # )
            self.roi_label_y_position = UILabel(
                relative_rect=pygame.Rect(self.roi_label_x_position.relative_rect.bottomleft, (60, COMPONENT_HEIGHT)),
                text='y:',
                manager=self.ui_manager,
                container=self
            )
            self.roi_text_y_position = UILabel(
                relative_rect=pygame.Rect(self.roi_label_y_position.relative_rect.topright,
                                          (self.rect.width - COMPONENT_PADDING - self.roi_label_y_position.rect.width,
                                           COMPONENT_HEIGHT)),
                text='placeholder',
                manager=self.ui_manager,
                container=self
            )
            # self.roi_text_y_position = UITextEntryLine(
            #     relative_rect=pygame.Rect(self.roi_label_y_position.relative_rect.topright,
            #                               (text_width - self.roi_label_y_position.rect.width, -1)),
            #     manager=self.ui_manager,
            #     container=self
            # )

            self.brush_slider_title, self.brush_slider, self.brush_slider_label = self.generate_slider(
                'brush:',
                pygame.Rect((COMPONENT_PADDING, self.roi_label_y_position.relative_rect.bottom + COMPONENT_PADDING),
                            (self.rect.width, COMPONENT_HEIGHT)),
                self,
                start_value=BRUSH_SIZE_DEFAULT,
                value_range=(BRUSH_SIZE_MIN, BRUSH_SIZE_MAX))
            self.alpha_slider_title, self.alpha_slider, self.alpha_slider_label = self.generate_slider(
                'alpha:',
                pygame.Rect(self.brush_slider_title.relative_rect.bottomleft, (self.rect.width, COMPONENT_HEIGHT)),
                self,
                start_value=ALPHA_DEFAULT,
                value_range=(ALPHA_MIN, ALPHA_MAX))
            # disable the alpha slider initially since there won't be any layers
            self.enable_alpha_slider(False)

            brush_button_width = (self.rect.width - COMPONENT_PADDING * 2) / 2
            self.brush_draw_button = UIButton(
                relative_rect=pygame.Rect(
                    self.alpha_slider_title.relative_rect.bottomleft,
                    (brush_button_width, COMPONENT_HEIGHT)),
                manager=self.ui_manager,
                container=self,
                text='draw')
            self.brush_draw_button.select()
            self.brush_erase_button = UIButton(
                relative_rect=pygame.Rect(
                    self.brush_draw_button.relative_rect.topright,
                    (brush_button_width, COMPONENT_HEIGHT)),
                manager=self.ui_manager,
                container=self,
                text='erase')
            self.add_layer_button = UIButton(
                relative_rect=pygame.Rect(
                    (COMPONENT_PADDING, self.brush_draw_button.relative_rect.bottom + COMPONENT_PADDING),
                    (self.rect.width - COMPONENT_PADDING * 2, COMPONENT_HEIGHT)),
                manager=self.ui_manager,
                container=self,
                text='add layer')

            self.layer_scrolling_container = UIScrollingContainer(
                relative_rect=pygame.rect.Rect(
                    self.add_layer_button.relative_rect.bottomleft,
                    (self.rect.width - COMPONENT_PADDING * 2, 200)),
                manager=self.ui_manager,
                container=self
            )

    def set_roi_labels(self, size: Tuple[int, int], resolution: Tuple[int, int]):
        self.roi_text_width.set_text(str(size[0]))
        self.roi_text_height.set_text(str(size[1]))
        self.roi_text_x_position.set_text(str(resolution[0]))
        self.roi_text_y_position.set_text(str(resolution[1]))

    def enable_alpha_slider(self, is_enabled: bool):
        if is_enabled:
            self.alpha_slider.set_current_value(255)
            self.alpha_slider.left_button.enable()
            self.alpha_slider.right_button.enable()
            self.alpha_slider.sliding_button.enable()
        else:
            self.alpha_slider.left_button.disable()
            self.alpha_slider.right_button.disable()
            self.alpha_slider.sliding_button.disable()

    def set_alpha_slider_value(self, alpha: int):
        self.alpha_slider.set_current_value(alpha)
        self.alpha_slider_label.set_text(str(alpha))

    def handle_add_layer_button(self, name: str, layer_type: str):
        container = UIContainer(
            relative_rect=pygame.Rect(
                (0, 0),
                (self.rect.width - COMPONENT_PADDING * 2 - MIN_COMPONENT_WIDTH, COMPONENT_HEIGHT)),
            manager=self.ui_manager,
            container=self.layer_scrolling_container.get_container()
        )

        button = UIButton(
            manager=self.ui_manager,
            container=container,
            relative_rect=pygame.Rect((0, 0), (container.rect.width - MIN_COMPONENT_WIDTH, container.rect.height)),
            allow_double_clicks=True,
            text=name
        )
        delete_button = UIButton(
            relative_rect=pygame.Rect(button.relative_rect.topright, (MIN_COMPONENT_WIDTH, COMPONENT_HEIGHT)),
            manager=self.ui_manager,
            container=container,
            text='╳'
        )
        layer_color_surface = pygame.Surface((int(MIN_COMPONENT_WIDTH * 0.6), int(COMPONENT_HEIGHT * 0.6)))
        layer_color_surface.fill(LAYER_OPTIONS[layer_type])
        layer_color = UIImage(relative_rect=pygame.Rect((int(MIN_COMPONENT_WIDTH * 0.2), int(COMPONENT_HEIGHT * 0.2)),
                                                        layer_color_surface.get_size()),
                              image_surface=layer_color_surface,
                              manager=self.ui_manager,
                              container=container)
        self.deselect_layer_buttons()
        button.select()
        self.layer_button_containers.append(container)
        self.layer_selection_buttons.append(button)
        self.layer_remove_buttons.append(delete_button)
        self.layer_button_colors.append(layer_color)
        self.reposition_layer_buttons()
        self.selected_layer_index = len(self.layer_button_containers) - 1

    def handle_update_layer_button(self, layer_index: int, layer_name: str, layer_type: str):
        self.layer_selection_buttons[layer_index].set_text(layer_name)
        layer_color = self.layer_button_colors[layer_index]
        layer_color_surface = pygame.Surface(layer_color.rect.size)
        layer_color_surface.fill(LAYER_OPTIONS[layer_type])
        layer_color.set_image(layer_color_surface)

    def handle_remove_layer_button(self, index):
        # TODO
        # button, delete_button = layer_buttons[index]
        # self.layer_buttons.pop(index)
        self.layer_selection_buttons.pop(index)
        self.layer_remove_buttons.pop(index)
        self.layer_button_colors.pop(index)
        container = self.layer_button_containers.pop(index)
        container.kill()
        # button.kill()
        # delete_button.kill()
        self.reposition_layer_buttons()

    def deselect_layer_buttons(self):
        for button in self.layer_selection_buttons:
            button.unselect()

    def reposition_layer_buttons(self):
        for i, container in enumerate(self.layer_button_containers):
            container.set_relative_position((0, container.rect.height * i))
            container.update_containing_rect_position()
        self.layer_scrolling_container.set_scrollable_area_dimensions(
            (self.rect.width - COMPONENT_PADDING * 2 - MIN_COMPONENT_WIDTH,
             len(self.layer_button_containers) * COMPONENT_HEIGHT))

    def set_brush_size(self, radius: int):
        self.brush_slider.set_current_value(radius)
        self.brush_slider_label.set_text(str(radius))

    def generate_slider(self, title, relative_rect, container, start_value, value_range):
        slider_title = UILabel(
            relative_rect=pygame.Rect((relative_rect.left, relative_rect.top), (50, relative_rect.height)),
            text=title,
            container=container,
            manager=self.ui_manager)
        slider = UIHorizontalSlider(
            relative_rect=pygame.Rect(
                slider_title.relative_rect.topright,
                (
                    relative_rect.width - slider_title.rect.width - MIN_COMPONENT_WIDTH - relative_rect.left * 2,
                    relative_rect.height)),
            container=container,
            manager=self.ui_manager,
            start_value=start_value,
            value_range=value_range)

        slider_label = UILabel(
            relative_rect=pygame.Rect(
                slider.relative_rect.topright,
                (MIN_COMPONENT_WIDTH, relative_rect.height)),
            text=str(int(slider.get_current_value())),
            container=container,
            manager=self.ui_manager)
        return slider_title, slider, slider_label

    def process_event(self, event: pygame.event.Event) -> bool:
        consumed_event = super(Sidebar, self).process_event(event)
        if event.type == pygame.USEREVENT:
            event_data = None
            if event.user_type == UI_HORIZONTAL_SLIDER_MOVED:
                if event.ui_element == self.image_slider:
                    slider_value = int(self.image_slider.get_current_value())
                    self.image_slider_label.set_text(str(slider_value))
                    selected_option = self.image_names_drop_down.options_list[slider_value - 1]
                    self.image_names_drop_down.selected_option = selected_option
                    self.image_names_drop_down.current_state.selected_option_button.set_text(selected_option)
                    event_data = {'user_type': EVENT_SIDEBAR_IMAGE_SLIDER_MOVED,
                                  'value': slider_value - 1}
                elif event.ui_element == self.brush_slider:
                    slider_value = int(self.brush_slider.get_current_value())
                    self.brush_slider_label.set_text(str(slider_value))
                    event_data = {'user_type': EVENT_SIDEBAR_BRUSH_SLIDER_MOVED,
                                  'value': slider_value}
                elif event.ui_element == self.alpha_slider:
                    # TODO: alpha
                    slider_value = int(self.alpha_slider.get_current_value())
                    self.alpha_slider_label.set_text(str(slider_value))
                    event_data = {'user_type': EVENT_SIDEBAR_ALPHA_SLIDER_MOVED,
                                  'alpha': slider_value,
                                  'layer_index': self.selected_layer_index}
            elif (event.user_type == UI_DROP_DOWN_MENU_CHANGED
                  and event.ui_element == self.image_names_drop_down):
                selected_option = self.image_names_drop_down.selected_option
                # TODO: duplicate names could possibly break this
                selected_option_index = self.image_names_drop_down.options_list.index(selected_option)
                self.image_slider_label.set_text(str(selected_option_index + 1))
                self.image_slider.set_current_value(selected_option_index + 1)
                event_data = {'user_type': EVENT_SIDEBAR_IMAGE_SLIDER_MOVED,
                              'value': selected_option_index}
            elif event.user_type == UI_BUTTON_PRESSED and event.ui_element == self.add_roi_button:
                event_data = {'user_type': EVENT_SIDEBAR_BUTTON_ADD_ROI}
            elif event.user_type == UI_BUTTON_PRESSED and event.ui_element == self.remove_roi_button:
                event_data = {'user_type': EVENT_SIDEBAR_BUTTON_REMOVE_ROI}
            elif event.user_type == UI_DROP_DOWN_MENU_CHANGED and event.ui_element == self.roi_selector:
                event_data = {'user_type': EVENT_SIDEBAR_SELECT_ROI,
                              'value': int(self.roi_selector.selected_option)}
            elif event.user_type == UI_BUTTON_PRESSED and event.ui_element == self.brush_draw_button:
                self.brush_draw_button.select()
                self.brush_erase_button.unselect()
                event_data = {'user_type': EVENT_SIDEBAR_BUTTON_DRAW_BUTTON}
            elif event.user_type == UI_BUTTON_PRESSED and event.ui_element == self.brush_erase_button:
                self.brush_draw_button.unselect()
                self.brush_erase_button.select()
                event_data = {'user_type': EVENT_SIDEBAR_BUTTON_ERASE_BUTTON}
            elif event.user_type == UI_BUTTON_PRESSED and event.ui_element == self.add_layer_button:
                event_data = {'user_type': EVENT_SIDEBAR_BUTTON_ADD_LAYER}
            elif event.user_type == UI_BUTTON_PRESSED and event.ui_element in self.layer_remove_buttons:
                layer_index = self.layer_remove_buttons.index(event.ui_element)
                self.handle_remove_layer_button(layer_index)
                event_data = {'user_type': EVENT_SIDEBAR_BUTTON_REMOVE_LAYER,
                              'layer_index': layer_index}
            elif event.user_type == UI_BUTTON_PRESSED and event.ui_element in self.layer_selection_buttons:
                self.selected_layer_index = self.layer_selection_buttons.index(event.ui_element)
                self.deselect_layer_buttons()
                event.ui_element.select()
                event_data = {'user_type': EVENT_SIDEBAR_SELECT_LAYER,
                              'layer_index': self.selected_layer_index}
            elif event.user_type == UI_BUTTON_DOUBLE_CLICKED and event.ui_element in self.layer_selection_buttons:
                layer_index = self.layer_selection_buttons.index(event.ui_element)
                self.deselect_layer_buttons()
                event.ui_element.select()
                event_data = {'user_type': EVENT_SIDEBAR_BUTTON_EDIT_LAYER,
                              'layer_index': layer_index}
            if event_data is not None:
                sidebar_event = pygame.event.Event(pygame.USEREVENT, event_data)
                pygame.event.post(sidebar_event)
        return consumed_event


class PreviewScreen(UIElement):
    def __init__(self,
                 relative_rect: pygame.Rect,
                 manager: IUIManagerInterface,
                 background: pygame.Surface,
                 roi_size: Tuple[int, int] = (800, 800),
                 # scale: float = 0.10,
                 container: Union[IContainerLikeInterface, None] = None,
                 parent_element: UIElement = None,
                 object_id: Union[str, None] = None,
                 anchors: Dict[str, str] = None
                 ):
        new_element_ids, new_object_ids = self._create_valid_ids(container=container,
                                                                 parent_element=parent_element,
                                                                 object_id=object_id,
                                                                 element_id='preview_screen')

        self.roi_position = (0, 0)
        image_size = background.get_size()
        self.scale = relative_rect.width / image_size[0]
        self.preview_size = (int(image_size[0] * self.scale), int(image_size[1] * self.scale))
        self.roi_size = roi_size
        self.background = pygame.transform.scale(background, self.preview_size)
        self.overlays_surface = pygame.Surface(self.preview_size, pygame.SRCALPHA)
        self.preview_screen = self.background.copy()
        self.roi_surface = pygame.Surface(self.preview_size, pygame.SRCALPHA)

        super().__init__(
            self.preview_screen.get_rect(left=relative_rect.left, top=relative_rect.top - self.preview_size[1]),
            manager,
            container,
            starting_height=1,
            layer_thickness=1,
            object_ids=new_object_ids,
            element_ids=new_element_ids,
            anchors=anchors)
        self.draw()

    def set_background(self, background: pygame.Surface):
        self.background = pygame.transform.scale(background, self.preview_size)
        self.draw()

    def set_overlays(self, overlays: List[pygame.Surface]):
        # self.overlays_surface.fill(0, 0, 0, 0)
        for overlay in overlays:
            self.overlays_surface.blit(pygame.transform.scale(overlay, self.preview_size), (0, 0))
        self.draw()

    def set_roi_size(self, size: Tuple[int, int]):
        self.roi_size = size
        self.draw()

    def set_roi_position(self, position: Tuple[int, int]):
        self.roi_position = position
        self.draw()

    def draw(self):
        self.preview_screen.blit(self.background, (0, 0))
        self.preview_screen.blit(self.overlays_surface, (0, 0))

        pygame.draw.rect(
            self.roi_surface,
            (0, 0, 255, 100),
            (self.roi_position[0] * self.scale, self.roi_position[1] * self.scale, self.roi_size[0] * self.scale,
             self.roi_size[1] * self.scale))
        self.preview_screen.blit(self.roi_surface, (0, 0), (0, 0, self.preview_size[0], self.preview_size[1]))
        self.roi_surface.fill((0, 0, 0, 0))
        self.set_image(self.preview_screen)


class RoiSelection(UIElement, IContainerLikeInterface):
    def __init__(self,
                 relative_rect: pygame.Rect,
                 manager: IUIManagerInterface,
                 container: Union[IContainerLikeInterface, None] = None,
                 parent_element: UIElement = None,
                 object_id: Union[str, None] = None,
                 anchors: Dict[str, str] = None
                 ):
        new_element_ids, new_object_ids = self._create_valid_ids(container=container,
                                                                 parent_element=parent_element,
                                                                 object_id=object_id,
                                                                 element_id='preview_screen')

        super().__init__(
            relative_rect,
            manager,
            container,
            starting_height=1,
            layer_thickness=1,
            object_ids=new_object_ids,
            element_ids=new_element_ids,
            anchors=anchors)

        self.container = UIContainer(relative_rect=relative_rect,
                                     manager=manager,
                                     container=container,
                                     parent_element=self)
        background = pygame.Surface(self.rect.size)

        background.fill(self.ui_manager.ui_theme.get_colour(None, None, 'dark_bg'))
        self.set_image(background)

        max_component_width = self.rect.width - COMPONENT_PADDING * 2
        self.roi_drop_down_menu = UIDropDownMenu(
            relative_rect=pygame.Rect((0, 0),
                                      (max_component_width, COMPONENT_HEIGHT)),
            manager=self.ui_manager,
            starting_option='',
            options_list=['500x500',
                          '600x600',
                          '700x700',
                          '800x800'],
            container=self,
            anchors={'left': 'left',
                     'right': 'right',
                     'top': 'top',
                     'bottom': 'bottom'}
        )
        self.roi_label_width = UILabel(
            relative_rect=pygame.Rect(
                self.roi_drop_down_menu.relative_rect.bottomleft,
                (60, COMPONENT_HEIGHT)),
            text='width:',
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_width = UITextEntryLine(
            relative_rect=pygame.Rect(self.roi_label_width.relative_rect.topright,
                                      (max_component_width - self.roi_label_width.rect.width, -1)),
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_width.set_allowed_characters('numbers')
        self.roi_text_width.set_text_length_limit(4)

        self.roi_label_height = UILabel(
            relative_rect=pygame.Rect(self.roi_label_width.relative_rect.bottomleft,
                                      (60, COMPONENT_HEIGHT)),
            text='height:',
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_height = UITextEntryLine(
            relative_rect=pygame.Rect(self.roi_label_height.relative_rect.topright,
                                      (max_component_width - self.roi_label_height.rect.width, -1)),
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_height.set_allowed_characters('numbers')
        self.roi_text_height.set_text_length_limit(4)

        self.roi_label_x_position = UILabel(
            relative_rect=pygame.Rect(self.roi_label_height.relative_rect.bottomleft,
                                      (60, COMPONENT_HEIGHT)),
            text='x:',
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_x_position = UITextEntryLine(
            relative_rect=pygame.Rect(self.roi_label_x_position.relative_rect.topright,
                                      (max_component_width - self.roi_label_x_position.rect.width, -1)),
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_x_position.set_allowed_characters('numbers')
        self.roi_text_x_position.set_text_length_limit(4)

        self.roi_label_y_position = UILabel(
            relative_rect=pygame.Rect(self.roi_label_x_position.relative_rect.bottomleft,
                                      (60, COMPONENT_HEIGHT)),
            text='y:',
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_y_position = UITextEntryLine(
            relative_rect=pygame.Rect(self.roi_label_y_position.relative_rect.topright,
                                      (max_component_width - self.roi_label_y_position.rect.width, -1)),
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_y_position.set_allowed_characters('numbers')
        self.roi_text_y_position.set_text_length_limit(4)

        self.preview_button = UIButton(
            relative_rect=pygame.Rect(
                (0,
                 self.roi_label_y_position.relative_rect.bottom + COMPONENT_PADDING),
                ((max_component_width - COMPONENT_PADDING) / 2, COMPONENT_HEIGHT)),
            text='Preview',
            manager=self.ui_manager,
            container=self)

        self.add_roi_button = UIButton(
            relative_rect=pygame.Rect(
                (self.preview_button.relative_rect.right + COMPONENT_PADDING,
                 self.preview_button.relative_rect.top),
                ((max_component_width - COMPONENT_PADDING) / 2, COMPONENT_HEIGHT)),
            text='Add',
            manager=self.ui_manager,
            container=self)

        self.cancel_button = UIButton(
            relative_rect=pygame.Rect(
                (0, self.preview_button.relative_rect.bottom + COMPONENT_PADDING),
                (max_component_width, COMPONENT_HEIGHT)),
            text='Cancel',
            manager=self.ui_manager,
            container=self)

        # self.set_roi_size((800, 800))
        # self.set_roi_position((0, 0))

    def get_container(self) -> IUIContainerInterface:
        return self.container

    def kill(self):
        self.get_container().kill()
        super().kill()

    def get_roi_size(self):
        roi_size = self.roi_drop_down_menu.selected_option.split('x')

        if len(roi_size) == 2:
            return tuple(roi_size)
        return '', ''

    def set_roi_size(self, size: Tuple[int, int]):
        self.roi_text_width.set_text(str(size[0]))
        self.roi_text_height.set_text(str(size[1]))

    def set_roi_position(self, position: Tuple[int, int]):
        self.roi_text_x_position.set_text(str(position[0]))
        self.roi_text_y_position.set_text(str(position[1]))

    def process_event(self, event: pygame.event.Event) -> bool:
        """
        Process any events relevant to the confirmation dialog.
        We close the window when the cancel button is pressed, and post a confirmation event
        (UI_CONFIRMATION_DIALOG_CONFIRMED) when the OK button is pressed, and also close the window.
        :param event: a pygame.Event.
        :return: Return True if we 'consumed' this event and don't want to pass it on to the rest
                 of the UI.
        """
        consumed_event = super().process_event(event)
        if event.type == pygame.USEREVENT:
            if (event.user_type == UI_BUTTON_PRESSED
                    and event.ui_element == self.cancel_button):
                event_data = {'user_type': EVENT_ROI_SELECTION_CANCEL,
                              'ui_element': self,
                              'ui_object_id': self.most_specific_combined_id}

                confirmation_dialog_event = pygame.event.Event(pygame.USEREVENT, event_data)
                pygame.event.post(confirmation_dialog_event)
                self.kill()

            if (event.user_type == UI_BUTTON_PRESSED
                    and event.ui_element == self.preview_button):
                roi_width = self.roi_text_width.get_text()
                roi_height = self.roi_text_height.get_text()
                roi_x = self.roi_text_x_position.get_text()
                roi_y = self.roi_text_y_position.get_text()
                if len(roi_width) > 2 and len(roi_height) > 2 and len(roi_x) > 0 and len(roi_y) > 0:
                    event_data = {'user_type': EVENT_ROI_SELECTION_PREVIEW,
                                  'ui_element': self,
                                  'ui_object_id': self.most_specific_combined_id,
                                  'size': (int(roi_width), int(roi_height)),
                                  'position': (int(roi_x), int(roi_y))}

                    confirmation_dialog_event = pygame.event.Event(pygame.USEREVENT, event_data)
                    pygame.event.post(confirmation_dialog_event)
                    self.kill()
                else:
                    return consumed_event

            if (event.user_type == UI_BUTTON_PRESSED
                    and event.ui_element == self.add_roi_button):
                roi_width = self.roi_text_width.get_text()
                roi_height = self.roi_text_height.get_text()
                roi_x = self.roi_text_x_position.get_text()
                roi_y = self.roi_text_y_position.get_text()
                if len(roi_width) > 2 and len(roi_height) > 2 and len(roi_x) > 0 and len(roi_y) > 0:
                    event_data = {'user_type': EVENT_ROI_SELECTION_ADD,
                                  'ui_element': self,
                                  'ui_object_id': self.most_specific_combined_id,
                                  'size': (int(roi_width), int(roi_height)),
                                  'position': (int(roi_x), int(roi_y))
                                  }

                    confirmation_dialog_event = pygame.event.Event(pygame.USEREVENT, event_data)
                    pygame.event.post(confirmation_dialog_event)
                    self.kill()
                else:
                    return consumed_event

            if (event.user_type == UI_DROP_DOWN_MENU_CHANGED
                    and event.ui_element == self.roi_drop_down_menu):
                roi_size = self.roi_drop_down_menu.selected_option.split('x')
                roi_width = ''
                roi_height = ''
                if len(roi_size) == 2:
                    roi_width = roi_size[0]
                    roi_height = roi_size[1]
                self.roi_text_width.set_text(roi_width)
                self.roi_text_height.set_text(roi_height)
                event_data = {'user_type': EVENT_ROI_SELECTION_PREVIEW,
                              'ui_element': self,
                              'ui_object_id': self.most_specific_combined_id,
                              'size': (int(roi_width), int(roi_height)),
                              'position': None}

                confirmation_dialog_event = pygame.event.Event(pygame.USEREVENT, event_data)
                pygame.event.post(confirmation_dialog_event)
            if (event.user_type == UI_TEXT_ENTRY_CHANGED
                    and event.ui_element in [self.roi_text_width, self.roi_text_height]):
                self.roi_drop_down_menu.selected_option = ''
                self.roi_drop_down_menu.current_state.selected_option_button.set_text('')
        return consumed_event


class RoiSelectionDialog(UIWindow):
    """
    A confirmation dialog that lets a user choose between continuing on a path they've chosen or
    cancelling. It's good practice to give a very brief description of the action they are
    confirming on the button they click to confirm it i.e. 'Delete' for a file deletion operation
    or, 'Rename' for a file rename operation.
    :param rect: The size and position of the window, includes the menu bar across the top.
    :param manager: The UIManager that manages this UIElement.
    :param window_title: The title of the  window.
    :param blocking: Whether this window should block all other mouse interactions with the GUI
                     until it is closed.
    :param object_id: A custom defined ID for fine tuning of theming. Defaults to
                      '#confirmation_dialog'.
    """

    def __init__(self, rect: pygame.Rect,
                 manager: IUIManagerInterface,
                 *,
                 window_title: str = 'Confirm',
                 blocking: bool = False,
                 object_id: str = '#roi_selection_dialog'):

        super().__init__(rect, manager,
                         window_display_title=window_title,
                         object_id=object_id,
                         resizable=True)

        minimum_dimensions = (260, 250)
        if rect.width < minimum_dimensions[0] or rect.height < minimum_dimensions[1]:
            warn_string = ("Initial size: " + str(rect.size) +
                           " is less than minimum dimensions: " + str(minimum_dimensions))
            warnings.warn(warn_string, UserWarning)
        self.set_minimum_dimensions(minimum_dimensions)

        self.confirm_button = UIButton(relative_rect=pygame.Rect(-220, -40, 100, 30),
                                       text='Preview',
                                       manager=self.ui_manager,
                                       container=self,
                                       object_id='#confirm_button',
                                       anchors={'left': 'right',
                                                'right': 'right',
                                                'top': 'bottom',
                                                'bottom': 'bottom'})

        self.save_button = UIButton(relative_rect=pygame.Rect(-110, -40, 100, 30),
                                    text='Save',
                                    manager=self.ui_manager,
                                    container=self,
                                    object_id='#add_roi_button',
                                    anchors={'left': 'right',
                                             'right': 'right',
                                             'top': 'bottom',
                                             'bottom': 'bottom'})

        text_width = self.get_container().get_size()[0] - 10
        text_height = self.get_container().get_size()[1] - 50
        self.roi_drop_down_menu = UIDropDownMenu(relative_rect=pygame.Rect(5, 5, text_width, COMPONENT_HEIGHT),
                                                 manager=self.ui_manager,
                                                 starting_option='',
                                                 options_list=['500x500',
                                                               '600x600',
                                                               '700x700',
                                                               '800x800'],
                                                 container=self,
                                                 anchors={'left': 'left',
                                                          'right': 'right',
                                                          'top': 'top',
                                                          'bottom': 'bottom'}
                                                 )
        self.roi_label_width = UILabel(
            relative_rect=pygame.Rect(self.roi_drop_down_menu.relative_rect.bottomleft, (60, COMPONENT_HEIGHT)),
            text='width:',
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_width = UITextEntryLine(
            relative_rect=pygame.Rect(self.roi_label_width.relative_rect.topright,
                                      (text_width - self.roi_label_width.rect.width, -1)),
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_width.set_allowed_characters('numbers')
        self.roi_text_width.set_text_length_limit(4)
        self.roi_label_height = UILabel(
            relative_rect=pygame.Rect(self.roi_label_width.relative_rect.bottomleft, (60, COMPONENT_HEIGHT)),
            text='height:',
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_height = UITextEntryLine(
            relative_rect=pygame.Rect(self.roi_label_height.relative_rect.topright,
                                      (text_width - self.roi_label_height.rect.width, -1)),
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_height.set_allowed_characters('numbers')
        self.roi_text_height.set_text_length_limit(4)

        self.roi_label_x_position = UILabel(
            relative_rect=pygame.Rect(self.roi_label_height.relative_rect.bottomleft, (60, COMPONENT_HEIGHT)),
            text='x:',
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_x_position = UITextEntryLine(
            relative_rect=pygame.Rect(self.roi_label_x_position.relative_rect.topright,
                                      (text_width - self.roi_label_x_position.rect.width, -1)),
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_x_position.set_allowed_characters('numbers')
        self.roi_text_x_position.set_text_length_limit(4)
        self.roi_label_y_position = UILabel(
            relative_rect=pygame.Rect(self.roi_label_x_position.relative_rect.bottomleft, (60, COMPONENT_HEIGHT)),
            text='y:',
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_y_position = UITextEntryLine(
            relative_rect=pygame.Rect(self.roi_label_y_position.relative_rect.topright,
                                      (text_width - self.roi_label_y_position.rect.width, -1)),
            manager=self.ui_manager,
            container=self
        )
        self.roi_text_y_position.set_allowed_characters('numbers')
        self.roi_text_y_position.set_text_length_limit(4)
        self.set_roi_size((800, 800))
        self.set_roi_position((0, 0))
        self.set_blocking(blocking)

    def get_roi_size(self):
        roi_size = self.roi_drop_down_menu.selected_option.split('x')

        if len(roi_size) == 2:
            return tuple(roi_size)
        return '', ''

    def set_roi_size(self, size: Tuple[int, int]):
        self.roi_text_width.set_text(str(size[0]))
        self.roi_text_height.set_text(str(size[1]))

    def set_roi_position(self, position: Tuple[int, int]):
        self.roi_text_x_position.set_text(str(position[0]))
        self.roi_text_y_position.set_text(str(position[1]))

    def process_event(self, event: pygame.event.Event) -> bool:
        """
        Process any events relevant to the confirmation dialog.
        We close the window when the cancel button is pressed, and post a confirmation event
        (UI_CONFIRMATION_DIALOG_CONFIRMED) when the OK button is pressed, and also close the window.
        :param event: a pygame.Event.
        :return: Return True if we 'consumed' this event and don't want to pass it on to the rest
                 of the UI.
        """
        consumed_event = super().process_event(event)
        if event.type == pygame.USEREVENT:
            if (event.user_type == UI_BUTTON_PRESSED
                    and event.ui_element == self.close_window_button):
                event_data = {'user_type': EVENT_ROI_SELECTION_CANCEL,
                              'ui_element': self,
                              'ui_object_id': self.most_specific_combined_id}

                confirmation_dialog_event = pygame.event.Event(pygame.USEREVENT, event_data)
                pygame.event.post(confirmation_dialog_event)
                self.kill()

            if (event.user_type == UI_BUTTON_PRESSED
                    and event.ui_element == self.confirm_button):
                roi_width = self.roi_text_width.get_text()
                roi_height = self.roi_text_height.get_text()
                roi_x = self.roi_text_x_position.get_text()
                roi_y = self.roi_text_y_position.get_text()
                if len(roi_width) > 2 and len(roi_height) > 2 and len(roi_x) > 0 and len(roi_y) > 0:
                    event_data = {'user_type': EVENT_ROI_SELECTION_PREVIEW,
                                  'ui_element': self,
                                  'ui_object_id': self.most_specific_combined_id,
                                  'size': (int(roi_width), int(roi_height)),
                                  'position': (int(roi_x), int(roi_y))}

                    confirmation_dialog_event = pygame.event.Event(pygame.USEREVENT, event_data)
                    pygame.event.post(confirmation_dialog_event)
                    self.kill()
                else:
                    return consumed_event

            if (event.user_type == UI_BUTTON_PRESSED
                    and event.ui_element == self.save_button):
                roi_width = self.roi_text_width.get_text()
                roi_height = self.roi_text_height.get_text()
                roi_x = self.roi_text_x_position.get_text()
                roi_y = self.roi_text_y_position.get_text()
                if len(roi_width) > 2 and len(roi_height) > 2 and len(roi_x) > 0 and len(roi_y) > 0:
                    event_data = {'user_type': EVENT_ROI_SELECTION_ADD,
                                  'ui_element': self,
                                  'ui_object_id': self.most_specific_combined_id,
                                  'size': (int(roi_width), int(roi_height)),
                                  'position': (int(roi_x), int(roi_y))
                                  }

                    confirmation_dialog_event = pygame.event.Event(pygame.USEREVENT, event_data)
                    pygame.event.post(confirmation_dialog_event)
                    self.kill()
                else:
                    return consumed_event

            if (event.user_type == UI_DROP_DOWN_MENU_CHANGED
                    and event.ui_element == self.roi_drop_down_menu):
                roi_size = self.roi_drop_down_menu.selected_option.split('x')
                roi_width = ''
                roi_height = ''
                if len(roi_size) == 2:
                    roi_width = roi_size[0]
                    roi_height = roi_size[1]
                self.roi_text_width.set_text(roi_width)
                self.roi_text_height.set_text(roi_height)
                event_data = {'user_type': EVENT_ROI_SELECTION_PREVIEW,
                              'ui_element': self,
                              'ui_object_id': self.most_specific_combined_id,
                              'size': (int(roi_width), int(roi_height)),
                              'position': None}

                confirmation_dialog_event = pygame.event.Event(pygame.USEREVENT, event_data)
                pygame.event.post(confirmation_dialog_event)
            if (event.user_type == UI_TEXT_ENTRY_CHANGED
                    and event.ui_element in [self.roi_text_width, self.roi_text_height]):
                self.roi_drop_down_menu.selected_option = ''
                self.roi_drop_down_menu.current_state.selected_option_button.set_text('')
        return consumed_event


class LayerDialog(UIWindow):
    """
    A confirmation dialog that lets a user choose between continuing on a path they've chosen or
    cancelling. It's good practice to give a very brief description of the action they are
    confirming on the button they click to confirm it i.e. 'Delete' for a file deletion operation
    or, 'Rename' for a file rename operation.
    :param rect: The size and position of the window, includes the menu bar across the top.
    :param manager: The UIManager that manages this UIElement.
    :param window_title: The title of the  window.
    :param blocking: Whether this window should block all other mouse interactions with the GUI
                     until it is closed.
    :param object_id: A custom defined ID for fine tuning of theming. Defaults to
                      '#confirmation_dialog'.
    """

    def __init__(self, rect: pygame.Rect,
                 manager: IUIManagerInterface,
                 *,
                 window_title: str = 'Confirm',
                 layer_name: str = '',
                 layer_type: str = '',
                 layer_index: Optional[int] = None,
                 edit_mode: bool = False,
                 blocking: bool = True,
                 object_id: str = '#layer_dialog'):

        super().__init__(rect, manager,
                         window_display_title=window_title,
                         object_id=object_id,
                         resizable=True)

        self.layer_index = layer_index
        self.edit_mode = edit_mode

        minimum_dimensions = (260, 225)
        if rect.width < minimum_dimensions[0] or rect.height < minimum_dimensions[1]:
            warn_string = ("Initial size: " + str(rect.size) +
                           " is less than minimum dimensions: " + str(minimum_dimensions))
            warnings.warn(warn_string, UserWarning)
        self.set_minimum_dimensions(minimum_dimensions)

        self.confirm_button = UIButton(relative_rect=pygame.Rect(-220, -40, 100, 30),
                                       text='Update' if self.edit_mode else 'Add',
                                       manager=self.ui_manager,
                                       container=self,
                                       object_id='#confirm_button',
                                       anchors={'left': 'right',
                                                'right': 'right',
                                                'top': 'bottom',
                                                'bottom': 'bottom'})

        self.cancel_button = UIButton(relative_rect=pygame.Rect(-110, -40, 100, 30),
                                      text='Cancel',
                                      manager=self.ui_manager,
                                      container=self,
                                      object_id='#cancel_button',
                                      anchors={'left': 'right',
                                               'right': 'right',
                                               'top': 'bottom',
                                               'bottom': 'bottom'})

        text_entry_width = self.get_container().get_size()[0] - 40 - COMPONENT_PADDING * 2

        self.layer_name_label = UILabel(
            relative_rect=pygame.Rect((COMPONENT_PADDING, COMPONENT_PADDING), (40, COMPONENT_HEIGHT)),
            text='name:',
            manager=self.ui_manager,
            container=self
        )
        self.layer_name_text = UITextEntryLine(
            relative_rect=pygame.Rect(self.layer_name_label.relative_rect.topright,
                                      (text_entry_width, -1)),
            manager=self.ui_manager,
            container=self
        )
        self.layer_name_text.set_text(layer_name)
        self.layer_type_label = UILabel(
            relative_rect=pygame.Rect((self.layer_name_label.relative_rect.left,
                                       self.layer_name_label.relative_rect.bottom + COMPONENT_PADDING),
                                      (40, COMPONENT_HEIGHT)),
            text='type:',
            manager=self.ui_manager,
            container=self
        )
        self.layer_type_drop_down_menu = UIDropDownMenu(
            relative_rect=pygame.Rect(self.layer_type_label.relative_rect.topright,
                                      (text_entry_width, COMPONENT_HEIGHT)),
            manager=self.ui_manager,
            starting_option=layer_type,
            options_list=LAYER_OPTIONS.keys(),
            container=self,
            anchors={'left': 'left',
                     'right': 'right',
                     'top': 'top',
                     'bottom': 'bottom'}
        )
        self.set_blocking(blocking)

    def process_event(self, event: pygame.event.Event) -> bool:
        """
        Process any events relevant to the confirmation dialog.
        We close the window when the cancel button is pressed, and post a confirmation event
        (UI_CONFIRMATION_DIALOG_CONFIRMED) when the OK button is pressed, and also close the window.
        :param event: a pygame.Event.
        :return: Return True if we 'consumed' this event and don't want to pass it on to the rest
                 of the UI.
        """
        consumed_event = super().process_event(event)
        if event.type == pygame.USEREVENT:
            if (event.user_type == UI_BUTTON_PRESSED
                    and event.ui_element == self.cancel_button):
                self.kill()

            if (event.user_type == UI_BUTTON_PRESSED
                    and event.ui_element == self.confirm_button):
                layer_name = self.layer_name_text.get_text()
                layer_type = self.layer_type_drop_down_menu.selected_option
                if len(layer_name) > 0 and len(layer_type) > 0:
                    user_type = EVENT_LAYER_DIALOG_UPDATE if self.edit_mode else EVENT_LAYER_DIALOG_CREATE
                    event_data = {'user_type': user_type,
                                  'ui_element': self,
                                  'ui_object_id': self.most_specific_combined_id,
                                  'layer_name': self.layer_name_text.get_text(),
                                  'layer_type': self.layer_type_drop_down_menu.selected_option,
                                  'layer_index': self.layer_index
                                  }

                    confirmation_dialog_event = pygame.event.Event(pygame.USEREVENT, event_data)
                    pygame.event.post(confirmation_dialog_event)
                    self.kill()
        return consumed_event


class LoadingMessageWindow(UIMessageWindow):
    def __init__(self,
                 manager: IUIManagerInterface,
                 html_message: str,
                 can_close=False,
                 rect=pygame.Rect((0, 0), (250, 160))):
        # Use the ui manager's resolution to position the message window
        window_resolution = manager.window_resolution
        message_window_rect = pygame.Rect(
            (
                # center from the left
                (window_resolution[0] / 2) - (rect.width / 2),
                # a fourth from the top
                (window_resolution[1] / 4) - (rect.height / 4)
            ),
            rect.size)
        super().__init__(rect=message_window_rect,
                         html_message=html_message,
                         manager=manager,
                         window_title='Loading...')
        if not can_close:
            self.set_blocking(True)
            self.dismiss_button.kill()
            self.close_window_button.disable()


class FileImportDialog(UIFileDialog):
    def __init__(self,
                 rect: pygame.Rect,
                 manager: IUIManagerInterface,
                 window_title: str = 'File Dialog',
                 initial_file_path: Union[str, None] = None,
                 object_id: str = '#file_dialog',
                 allow_existing_files_only: bool = False,
                 allow_picking_directories: bool = False
                 ):
        super().__init__(
            rect=rect,
            manager=manager,
            window_title=window_title,
            initial_file_path=initial_file_path,
            object_id=object_id,
            allow_existing_files_only=allow_existing_files_only,
            allow_picking_directories=allow_picking_directories
        )

        self.file_selection_list.set_dimensions(
            (self.file_selection_list.rect.width / 2 - 50, self.file_selection_list.rect.height))
        self.add_button = UIButton(
            relative_rect=pygame.Rect(
                (self.file_selection_list.relative_rect.right,
                 self.file_selection_list.relative_rect.height / 2 + self.file_selection_list.relative_rect.top - 30),
                (100, 30)),
            text='Add',
            manager=self.ui_manager,
            container=self)
        self.remove_button = UIButton(
            relative_rect=pygame.Rect(
                self.add_button.relative_rect.bottomleft,
                (100, 30)),
            text='Remove',
            manager=self.ui_manager,
            container=self)

        self.selected_files = []
        self.selected_files_list = UISelectionList(
            relative_rect=pygame.Rect((self.add_button.relative_rect.right, self.file_selection_list.relative_rect.top),
                                      self.file_selection_list.rect.size),
            item_list=self.selected_files,
            manager=self.ui_manager,
            container=self
        )

    def process_event(self, event: pygame.event.Event) -> bool:
        handled = super().process_event(event)

        self._process_add_remove_events(event)

        return handled

    def _process_add_remove_events(self, event):
        """
        Handle what happens when you press add and remove.

        :param event: event to check.

        """
        if (event.type == pygame.USEREVENT and event.user_type == UI_BUTTON_PRESSED
                and event.ui_element == self.remove_button):
            selected_index_list = [self.selected_files_list.item_list.index(item) for item in
                                   self.selected_files_list.item_list if item['selected']]
            if len(selected_index_list) > 0:
                self.selected_files.pop(selected_index_list[0])
                self.selected_files_list.set_item_list([f.split('/')[-1] for f in self.selected_files])

            # if selected_file is not None:
            #     self.selected_files.remove(selected_file)
        if (event.type == pygame.USEREVENT and event.user_type == UI_BUTTON_PRESSED
                and event.ui_element == self.add_button
                and self._validate_file_path(self.current_file_path)):
            current_file_path = str(self.current_file_path)
            if not current_file_path in self.selected_files:
                self.selected_files.append(str(self.current_file_path))
                self.selected_files_list.set_item_list([f.split('/')[-1] for f in self.selected_files])

    def _process_ok_cancel_events(self, event):
        """
        Handle what happens when you press OK and Cancel.

        :param event: event to check.

        """
        if (event.type == pygame.USEREVENT and event.user_type == UI_BUTTON_PRESSED
                and event.ui_element == self.cancel_button):
            self.kill()
        if (event.type == pygame.USEREVENT and event.user_type == UI_BUTTON_PRESSED
                and event.ui_element == self.ok_button):
            event_data = {'user_type': UI_FILE_DIALOG_PATH_PICKED,
                          'files': self.selected_files,
                          'ui_element': self,
                          'ui_object_id': self.most_specific_combined_id}
            new_file_chosen_event = pygame.event.Event(pygame.USEREVENT, event_data)
            pygame.event.post(new_file_chosen_event)
            self.kill()
